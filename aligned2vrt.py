# -*- coding: utf-8 -*-

# Converts sentence-aligned files (in LF Aligner / hunalign format:
# one chunk per line, languages separated by tab) to the VRT format.
# Files are tagged by default (by the TreeTagger)
#
# INPUT: file or directory containing data files
# OUTPUT: file(s) converted to the VRT format

# Michal Wozniak (michauwww@gmail.com)
# v. 1.0
# Created: 16.09.2019
# Last modification: -

import argparse
import os
import re
import subprocess
import glob
try:                    # import sent_ and word_tokenize from the NLTK package if installed else their simple replacements
    from nltk import sent_tokenize, word_tokenize
except ImportError:
    def sent_tokenize (text):
        sents = []
        for match in re.finditer ('(.*?)([.!?])( |$)', text):
            sents.append (match.group (1) + match.group (2))

        return sents

    def word_tokenize (text):
        text = text.strip ()
        tokens = []
        pat = re.compile (r'([(<(\[?])?([^ \t,?!:;()<>\[\]]*)([,?!:;)>\]])?(?=[ ,.!?"([<]|$)')
        text = text.replace ('!#!', '@')
        for token in re.finditer(pat, text):
            tokens += [tok for tok in token.groups() if tok and tok.strip ()]

        return tokens

def treeTaggerCommand (lang, temp_name):

    PATH_TO_BIN = '/opt/ttagger/bin/tree-tagger'
    PATH_TO_LIB = '/opt/treetaggerlib'

    command = '%s -token -lemma -sgml %s/%s.par %s' % (PATH_TO_BIN, PATH_TO_LIB, lang.lower (), temp_name)

    return command.split ()

def get_files (path, recursive, extensions = 'all'):
    if os.path.isfile (path):
        return [path]
    if recursive:
        filelist = [os.path.join (root, fname) for root, dirs, files in os.walk (path) for fname in files]
    else:
        filelist = [f for f in glob.glob (os.path.join (path, '*')) if os.path.isfile (f)]
    if extensions != 'all':
        filelist = [f for f in filelist if os.path.splitext (f)[1] in extensions]

    return filelist

def make_output_list (filelist, basepath, outpath, remove = []):
    outfiles = []
    basepath.rstrip ('/')
    outpath.rstrip ('/')
    print ('basepath:', basepath)
    print ('outpath:', outpath)

    for f in filelist:
        dir, fname = os.path.split (f)
        print ('dir before:', dir)
        dir = dir.replace (basepath, outpath)
        print ('dir after:', dir)
        if not os.path.exists (dir):
            os.makedirs (dir)
        for r in remove:
            fname = fname.replace (r, '')
        fname = os.path.splitext (fname)[0]
        outfiles.append (os.path.join (dir, fname))

    return outfiles

def getTextsWithSentences (path, languages):
    texts = {lang: [] for lang in languages}
    s_id = {lang: 0 for lang in languages}
    with open (path, encoding = 'utf8') as fin:
        for line in fin:
            chunks = line.strip ().split ('\t')
            assert len (chunks) >= len (languages)
            for lang, chunk in zip (languages, chunks):
                sentences = sent_tokenize (chunk)
                texts[lang].append ('<Align>')
                for sent in sentences:
                    s_id[lang] += 1
                    texts[lang].append ('<s id="%d">' % s_id[lang])
                    texts[lang].extend (word_tokenize (sent))
                    texts[lang].append ('</s>')
                if not sentences:
                    s_id[lang] += 1
                    texts[lang].append ('<s id="%d">' % s_id[lang])
                    texts[lang].append ('--')
                    texts[lang].append ('</s>')
                texts[lang].append ('</Align>')
    for lang in languages:
        texts[lang] = '\n'.join (texts[lang])

    return texts

def tagTexts (texts):

    temp_file_name = 'tree-tagger.tmp'
    tagged_texts = {}

    for lang, text in texts.items ():
        command = treeTaggerCommand (lang, temp_file_name)
        with open (temp_file_name, 'w', encoding = 'utf8') as ftmp:
            ftmp.write (text)
        tagger = subprocess.Popen (command, stdout = subprocess.PIPE)
        tagged_texts[lang] = tagger.communicate ()[0].decode ('utf8')

    return tagged_texts

def makeCorpus (inlist, outlist, languages, tag = True):
    for infpath, outpath in zip (inlist, outlist):
        texts = getTextsWithSentences (infpath, languages)
        if tag:
            texts = tagTexts (texts)
        import pickle
        with open ('texts.dat', 'wb') as fpickle:
            pickle.dump (texts, fpickle)
        for lang in languages:
            print ('o:', outpath)
            with open ('%s_%s.vrt' % (outpath, lang), 'w', encoding = 'utf8') as fout:
                fout.write ('<doc id="%s">\n' % os.path.split (outpath)[1])
                fout.write (texts[lang])
                fout.write ('</doc>')

if __name__ == '__main__':
    parser = argparse.ArgumentParser ()
    parser.add_argument ('input')
    parser.add_argument ('-l', '--languages', nargs = '+', required = 'store_true')
    parser.add_argument ('-e', '--extensions', nargs = '+', default = ['.txt'])
    parser.add_argument ('-r', '--recursive', action = 'store_true')
    parser.add_argument ('-o', '--output')
    parser.add_argument ('-s', '--separator', default = '\t')
    parser.add_argument ('-d', '--delete', nargs = '+', default = [])
    parser.add_argument ('-nt', '--no-tagging', action = 'store_true')

    args = parser.parse_args ()

    filelist = get_files (args.input, args.recursive, args.extensions)
    outputlist = make_output_list (filelist, args.input, args.output, remove = args.delete)
    makeCorpus (filelist, outputlist, args.languages, tag = not args.no_tagging)
