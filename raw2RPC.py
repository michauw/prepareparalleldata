## Script prepares raw text in order to be use in ParaVoz:
## Raw text is sentence tokenized, tagged (by TreeTagger)
## and converted to RPC format

# v 1.1 11.10.2016 
# changes: fixed quote function bug 

import os, sys
import nltk
from nltk.tokenize import sent_tokenize as st
from nltk.tokenize import word_tokenize
from subprocess import check_output
import subprocess
import argparse

SENT_MARKER = '##sent##'
UNKNOWN_TAG = '-'
TAGGER_PATH = 'tree-tagger'

def quote (text, onlyQuote = False):
    if onlyQuote and '"' in text:
        return re.sub ('"', r'&#34;', text)
    restricted = {"'": r'&#39;', '"': r'&#34;', '&': r'&#38;', '<': r'&#60;', '>': r'&#62;'}
    newText = ''
    for i in range (len (text)):
        if text[i] in restricted:
            newText += restricted[text[i]]
        else:
            newText += text[i]
    return newText

def unbom (text, encoding):
    if encoding == 'utf8':
        BOM = '\xef\xbb\xbf'
    elif encoding == 'utf16':
        BOM = '\xff\xfe'
    else:
        return text
    if text.startswith (BOM):
        print 'bim bam BOM!!!'
        return text[len (BOM):]
    else:
        return text

def tokenize (fname, lang = 'en', eof_bound = True, sent_marker = False, encoding = 'utf8', join = False):
    
    '''Sentence tokenizer. Uses NLTK punkt tokenizer. 
If eof_bound is set it will also treat end of line characters as a sentence separator.'''
    
    model = 'tokenizers/punkt/english.pickle'
    tokenizer = nltk.data.load (model)
    with open (fname) as fin:
        text = fin.read ()
        text = unbom (text, encoding)
        tokenized = tokenizer.tokenize (text.decode (encoding))
    new = []

    for sentence in tokenized:
        if eof_bound:
            splitted = sentence.strip ().split (os.linesep)
            for s in splitted:
                if s.strip ():
                    new += [s, SENT_MARKER]
        else:
            new += [sentence, SENT_MARKER]
    
    if not join:
        return new
    else:
        return '\n'.join (new)

def tag (text, lang):
    
    '''Tag the text using TreeTagger'''
    
    command = [os.path.join (TAGGER_PATH, 'bin', 'tree-tagger'), '-token', '-lemma', '-sgml', os.path.join (TAGGER_PATH, 'lib', lang + '.par')]
    print subprocess.list2cmdline (command)
    with open ('tmp.txt', 'w') as ftmp:
        ftmp.write (text.encode ('utf8'))
    with open ('tmp.txt') as ftmp, open (os.devnull, 'w') as fnull:
        p2 = subprocess.Popen (command, stdin = ftmp, stdout = subprocess.PIPE, stderr = fnull)
    results = p2.communicate ()[0]
    #os.remove ('tmp.txt')
    
    return results
    
def fakeTagging (sentences, encoding = 'utf8'):
    text = ''
    for sentence in sentences:
        words = word_tokenize (sentence)
        text += '\n'.join (words + [SENT_MARKER]) + '\n'
    return text.encode (encoding)
    
def toRPC (tagged_text, outname, meta = ''):
    
    header = '<TEI2>\n<teiHeader>\n<fileDesc>\n%s</fileDesc>\n</teiHeader>\n<text>\n<body>\n<div>\n' % meta
    footer = '\n</div>\n</body>\n</text>\n</TEI2>'
    text = '<s id="0">'
    sent_id = 0

    for ind, line in enumerate (tagged_text.splitlines ()):
        if line.startswith (SENT_MARKER) or line == '\n':
            sent_id += 1
            text += '</s>\n<s id="%d">' % sent_id
        else:
            els = line.split ('\t')
            if len (els) not in [1, 3]:
                print 'line not processed correctly by TreeTagger:"%s"' % line
                continue
            elif len (els) == 1:
                text += '<tok>%s</tok>' % quote (els[0])
            else:
                if els[2] == '<unknown>':
                    els[2] = UNKNOWN_TAG
                text += '<tok>%s<lemma>%s</lemma><tag1>%s</tag1></tok>' % (quote (els[0]), quote(els[2]), quote (els[1]))
    text = text[:-10 - len (str (sent_id))]
    with open (outname, 'w') as fout:
        text = header + text + footer
        fout.write ((header + text + footer))
        
def getMeta (metaFile, fields = [], sep = '\t'):
    
    metaDict = {}
    with open (metaFile) as fmeta:
        if not fields:
            fields = fmeta.readline ().strip ().split (sep)
        for line in fmeta:
            fs = line.strip ().split (sep)
            metaDict[fs[0]] = fs[1:]
    return [metaDict, fields]
    
def getFullLang (lang):
    langs = [('english', 'en'), ('french', 'fr'), ('spanish', 'es'), 
             ('german', 'de'), ('polish', 'pl'), ('russian', 'ru'), 
             ('ukrainian', 'uk'), ('yiddish', 'yi'), ('silessian', 'sz')]
    
    for full, abbr in langs:
        if lang == full or lang == abbr:
            fulllang = full
            abbrlang = abbr
            return fulllang, abbrlang
    
    return None, None
    
        
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser ()
    parser.add_argument ('-i', '--input')
    parser.add_argument ('-o', '--output', default = '.')
    parser.add_argument ('-l', '--lang')
    parser.add_argument ('-m', '--meta-file')
    parser.add_argument ('-r', '--recursive', action = 'store_true')
    parser.add_argument ('-a', '--auto', action = 'store_true')
    parser.add_argument ('-e', '--encoding', default = 'utf8')
    args = parser.parse_args ()
    # try:
        # fname, lang = sys.argv[1:3]
    # except IndexError:
        # print 'To few parameters!'
        # sys.exit (0)
    # if len (sys.argv) > 3:
        # tname = sys.argv[3]
    # else:
        # tname = os.path.splitext (os.path.split (fname)[1])[0]
    
    if not args.auto:
        fulllang, abbrlang = getFullLang (args.lang.lower ())
        if not fulllang:
            print 'Language (%s) not recognized!' % lang
            sys.exit (0)        
    
    if os.path.isdir (args.input):
        if args.recursive:
            files = []
            for root, dirs, fls in os.walk (args.input):
                files += [os.path.join (root, f) for f in fls]
        else:
            files = [os.path.join (args.input, fname) for fname in os.listdir (args.input)]
        if args.output != '.' and not os.path.exists (args.output):
            os.makedirs (args.output)
    elif os.path.isfile (args.input):
        files = [args.input]
    else:
        print 'Path not found:', args.input
        sys.exit (0)
        
    if args.meta_file:
        meta = getMeta (args.meta_file)
    else:
        meta = None
        
    for fname in files:
        print fname
        tname = os.path.splitext (os.path.split (fname)[1])[0]
        if args.auto:
            try:
                fileid, lang = tname.split ('_')
            except ValueError:
                print "File format error, skipping: ", fname
                continue
            fulllang, abbrlang = getFullLang (lang.lower ())
            if not fulllang:
                print "Language (%s) not recognized, skipping: %s" % (lang, fname)
                continue
        else:
            fileid = tname.split ('_')[0]
        #print 'Sentence tokenization...'
        taggerPath = os.path.join (TAGGER_PATH, 'bin', 'tree-tagger')
        taggingPossible = os.path.exists (taggerPath)
        sent_tokenized = tokenize (fname, fulllang, sent_marker = not taggingPossible, encoding = args.encoding)
        #print 'Tagging...'
        if taggingPossible:
            text = []
            for sent in sent_tokenized:
                words = word_tokenize (sent) if sent.strip () != SENT_MARKER else [sent.strip ()]
                text += [el + '\n' for el in words]
                text.append ('\n')
            text = ''.join (text)
                
            tagged = tag (text, fulllang)
        else:
            print 'cannot tag %s (treetagger not found or missing parameter file), tagger file: %s' % (fulllang, taggerPath)
            tagged = fakeTagging (sent_tokenized, args.encoding)
        #print 'Converting to RPC...'
        
        if os.path.isfile (args.input):
            if args.output == '.':
                outname = (fileid + '_' + abbrlang).upper () + '.rpc'
            else:
                outname = args.output
        else:
            outname = os.path.join (args.output, (fileid + '_' + abbrlang).upper () + '.rpc')
        if meta:
            try:
                match = meta[0][fileid]
                metadata = '<fileName>' + os.path.split (fname)[1] + '</fileName>\n'
                for i in range (1, len (meta[1])):
                    metadata += '<%s>%s</%s>\n' % (meta[1][i], match[i-1], meta[1][i])
            except KeyError:
                print "Could not find metadata for file: %s (fileid: %s)" % (fname, fileid)
        else:
            metadata = ''
        toRPC (tagged, outname, metadata)
    print 'Done.'
