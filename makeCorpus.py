## v 1.1 28.09.2016

import os, sys, argparse, shutil, re, glob

HUN_PATH = 'hunalign-1.2/src/hunalign/hunalign'
UPLUG_PATH = '/home/michalw/uplug/uplug'
CWB_PATH = 'CWBLRGLN/cwb/utils'
VERBOSE = False
SCRIPT_PATH = os.path.join (os.path.abspath ('.'), 'scripts')
RPC_PATH = ''
JAVA_COMM = 'java -Xms2500m -jar ' + os.path.join (SCRIPT_PATH, 'saxon9he.jar')
DESTINATION = '.'
TEMP_DIR = '.'
TEMP_NAME = 'TEMP_CORPUS'
TEMP_PATH = ''

## helpers ##

def findTexts (langsToFind = []):
    global RPC_PATH
    if not os.path.exists (RPC_PATH):
        print "Couldn't find source path: ", RPC_PATH
        sys.exit (1)
    curdir = os.path.abspath ('.')
    os.chdir (RPC_PATH)
    texts = {}
    files = []
    makeCopy = False
    pattern = r'^([^_]+)_([^.]+)\.rpc$'
    for fname in [elem for elem in os.listdir ('.') if os.path.isfile (elem)]:
        match = re.search (pattern, fname, re.I)
        if match:
            text, lang = match.groups ()
            if langsToFind and not lang in langsToFind:
                makeCopy = True
                continue
            try:
                texts[text].append (lang)
            except KeyError:
                texts[text] = [lang]
            files.append (fname)
    if makeCopy:
        RPC_PATH = os.path.join (SCRIPT_PATH, 'RPC-SELECTED')
        try:
            os.mkdir (RPC_PATH)
        except IOError:
            print "Couldn't create temporary directory (permission problem?)"
            sys.exit (1)
        except OSError:
            pass
        for fn in files:
            shutil.copy (fn, RPC_PATH)
    for text, langs in texts.items ():
        if len (langs) < 2:
            print "%s: there's only one language - ignoring..." % text
            del texts[text]

    if not texts:
        print "Couldn't find any file matching pattern (should be: TEXT-NAME_LANG.rpc, case insensitive)"
        sys.exit (1)
    longest = len (max (texts.keys ()))
    print 'Found texts:'
    for text, langs in texts.items ():
        print '%s:  [ %s ]' % (text.ljust (longest), ' '.join (sorted (langs)))
    print
    os.chdir (curdir)
    return texts
    
def findLanguages (rpc_path, includeLangVariants = True):
    if includeLangVariants:
        pat = r'^[^_]+_([A-Z]+[a-z]?)\.rpc(\.xml)?'
    else:
        pat = r'^[^_]+_([A-Z]+)[a-z]?\.rpc(\.xml)?'
    langs = set ()
    for fname in os.listdir (rpc_path):
        r = re.search (pat, fname)
        if not r:
            print 'findLanguages: Warning! Filename "%s" does not match the pattern...' % fname
            continue
        langs.add (r.group (1).lower ())
    
    return sorted (langs)

def getNewLangs (dr, langs):
    done = set ()
    for subd in [el for el in os.listdir (dr) if os.path.isdir (el)]:
        ldr = subd.split ('-')
        for lang in ldr:
            done.add (lang)
    return list (set (langs).difference (done))
    
def changeVariantFilenames (dir):
    print 'cvf', dir
    pat = r'^([\w]+)=(\w+)=(\w+)=.*\.alg\.xml'
    for fname in os.listdir (dir):
        r = re.search (pat, fname)
        if not r:
            continue
        newName = re.sub (pat, r'\1_\2-\3.alg.xml', fname)
        print 'Changing: %s --> %s' % (fname, newName)
        #shutil.copy (os.path.join (dir, fname), os.path.join (dir, newName))
        with open (os.path.join (dir, fname)) as fin:
            text = fin.read ()
        text = re.sub (r'fromDoc="([^/]+)/', r'fromDoc="%s/' % r.group (2).lower (), text)
        text = re.sub (r'toDoc="([^/]+)/', r'toDoc="%s/' % r.group (3).lower (), text)
        text = re.sub (r'((?:to|from)Doc="[^/]+/)([\w]+)=[^".]*\.xml"', r'\1\2.xml"', text)
        with open (os.path.join (dir, newName), 'w') as fout:
            fout.write (text)

def makePreparations (stages, args):
    texts = findTexts ()
    allOk = checkPrerequisites (stages, texts)
    if not allOk:
        sys.exit (1)
    try:
        if TEMP_DIR:
            temp_path = os.path.join (TEMP_DIR, TEMP_NAME)
        os.makedirs (temp_path)
    except OSError:
        pass
    os.chdir (temp_path)

    return texts

def checkPrerequisites (stages, texts):

    global SCRIPT_PATH
    global RPC_PATH
    global HUN_PATH
    global CWB_PATH

    allOk = True
    if os.system ('which perl > /dev/null'):
        print 'Error: perl not found'
        allOk = False
    if set (stages).intersection (set ([2, 5])):
        if os.system ('which java > /dev/null'):
            print 'Error: java not found'
            allOk = False
    if not os.path.exists (SCRIPT_PATH):
        print 'Error: script directory (%s) not found' % SCRIPT_PATH
        allOk = False
    if set (stages).intersection (set ([1, 2, 4])) and not os.path.exists (RPC_PATH):
        print 'Error: source files directory (%s) not found' % RPC_PATH
        allOk = False
    reqFiles = {1: ['BuildCorpusHunalignMW.pl', 'Prepare-RPC-for-HunalignSERVER.pl', 'RPC_XML_Tags.pm', 'AdjustAlign.py'],
                2: ['RPC2OpusCORPUS.xslt',  'RPC2OpusALIGN.xslt', 'MultiDirRpcXML2Uplug.pl'],
                4: ['AdjustLinkFiles.pl', 'generateAllBashFilesMW.pl'],
                5: ['GeneralizedLink2CWB.xslt', 'AdjustCWBFiles.pl']}
    reqDirs = {2: ['Texte-ALG-TST'], 4: []}
    for text in texts.keys ():
        reqDirs[4].append ('Align_' + text)

    if 1 in stages:
        if os.system ('which python > /dev/null'):
            print 'Error: python not found'
            allOk = False
        if not os.path.exists (HUN_PATH):
            print 'Error: path to hunalign (%s) not found' % HUN_PATH
            allOk = False
    if 6 in stages:
        if not os.path.exists (CWB_PATH):
            print 'Error: path to CWB (%s) not found' % CWB_PATH
            allOk = False
    for stage in stages:
        try:
            for file in reqFiles[stage]:
                if not os.path.exists (file) and not os.path.exists (os.path.join (SCRIPT_PATH, file)):
                    print "Error: file '%s' not found!" % file
                    allOk = False
        except KeyError:
            pass
    try:
        for dr in reqDirs[stages[0]]:
            if not os.path.exists (dr) and not os.path.exists (os.path.join (TEMP_PATH, dr)):
                print "Error: directory '%s' not found!" % dr
                allOk = False
    except KeyError:
        pass
    if not allOk:
        print 'Errors found. Script will be terminated. Sorry!'
    return allOk

## stages ##

def hunThemAll ():

    # stage 1 - hunalign

    global SCRIPT_PATH
    global RPC_PATH
    print 'Stage 1: Hunalign: sentence align'
    workingDir = os.path.abspath ('.')
    os.chdir (SCRIPT_PATH)
    comm1 = 'perl %s %s  -full -wdir=%s -odir=%s -hunpath=%s' % ('BuildCorpusHunalignMW.pl', RPC_PATH, workingDir + os.sep + 'HUNALIGNING', workingDir + os.sep + 'Texte-ALG-TST', HUN_PATH)
    comm2 = 'python %s %s' % ('AdjustAlign.py', workingDir + os.sep + 'Texte-ALG-TST')
    os.system (comm1)
    os.system (comm2)
    os.chdir (workingDir)
    print 'Finished sentence aligning'

def hun2uplug (texts, concatenate = False, cont = False):

    # stage 2 - convert sentence aligned files to uplug format

    global SCRIPT_PATH
    global RPC_PATH
    global JAVA_COMM
    print 'Stage 2: Preparing files for word alignment...'
    params = {'rpc': RPC_PATH,
              'outShort': 'Texte-TEI-SHORTTAG',
              'outShortAbs': os.path.join (os.path.abspath ('.'), 'Texte-TEI-SHORTTAG'),
              'outLong': 'Texte-rpc-TEI',
              'outLongAbs': os.path.join (os.path.abspath ('.'), 'Texte-rpc-TEI'),
              'outAlg': 'Texte-ALG-TEI',
              'outAlgAbs': os.path.join (os.path.abspath ('.'), 'Texte-ALG-TEI'),
              'alignPath': os.path.join (os.path.abspath ('.'), 'Align_All'),
              'langs': '',
              'opCorpus': os.path.join (SCRIPT_PATH, 'RPC2OpusCORPUS.xslt'),
              'opAlign': os.path.join (SCRIPT_PATH, 'RPC2OpusALIGN.xslt'),
              'wlist': os.path.join (SCRIPT_PATH, 'WriteListFile.pl'),
              'concat': os.path.join (SCRIPT_PATH, 'ConcatFilesForWordalignment.xslt'),
              'flist': os.path.join (os.path.abspath ('.'), 'FILELIST.xml'),
              'java': JAVA_COMM}
    comm1 = '%(java)s %(rpc)s -o:%(outShort)s %(opCorpus)s short="yes"'
    comm2 = '%(java)s %(rpc)s -o:%(outLong)s %(opCorpus)s short="no"'
    comm3 = '%(java)s Texte-ALG-Flat %(opAlign)s -o:%(outAlg)s'
    comm4 = 'perl MultiDirRpcXML2Uplug.pl %s %s'
    comm5 = 'perl %(wlist)s %(outLong)s'
    comm6 = '%(java)s %(concat)s %(concat)s TEICorpusPath="%(outShortAbs)s" TEIAlignPath="%(outAlgAbs)s" OUTPUTPATH="%(alignPath)s" FILELIST="%(flist)s" lngs="%(langs)s"'

    try:
        os.mkdir ('Texte-TEI-SHORTTAG')
        os.mkdir ('Texte-rpc-TEI')
        os.mkdir ('Texte-ALG-Flat')
        os.mkdir ('Texte-ALG-TEI')
    except OSError:
        pass

    if cont:
        outShort = params['outShort']
        outLong = params['outLong']
        for fl in os.listdir (RPC_PATH):
            text = os.path.splitext (fl)[0]
            params['rpc'] = os.path.join (RPC_PATH, fl)
            params['outShort'] = os.path.join (outShort, text + '.rpc.xml')
            params['outLong'] = os.path.join (outLong, text + '.rpc.xml')
            if not os.path.exists (params['outShort']):
                os.system (comm1 % params)
            if not os.path.exists (params['outLong']):
                os.system (comm2 % params)
    else:
        os.system (comm1 % params)
        os.system (comm2 % params)
    for fl in glob.glob ('Texte-ALG-TST/*/*.alg'):
        print os.path.abspath ('.')
        shutil.copy (fl, 'Texte-ALG-Flat')
    if cont:
        outAlg = params['outAlg']
        for fl in os.listdir ('Texte-ALG-Flat'):
            text = os.path.spltext (fl)[0]
            params['outAlg'] = os.path.join (outAlg, text + '.alg.xml')
            if not os.path.exists (params['outAlg']):
                os.system (comm3 % params)
    else:
        os.system (comm3 % params)
        
    changeVariantFilenames (params['outAlg'])       
    if concatenate:
        print 'Concatenating...'
        print comm5 % params
        print os.path.abspath ('.')
        os.system (comm5 % params)
        langs = findLanguages (params['rpc'])
        params['langs'] = ' '.join (langs)
        print comm6 % params
        os.system (comm6 % params)
        #os.remove ('FILELIST.xml')
    else:   
        workingDir = os.path.abspath (os.curdir)
        os.chdir (SCRIPT_PATH)          
        for text in texts.keys ():
            dr = workingDir + os.sep + 'Align_' + text
            try:
                os.mkdir (dr)
            except OSError:
                pass
            for fl in glob.glob (workingDir + os.sep + 'Texte-TEI-SHORTTAG/' + text + '*.xml') + glob.glob (workingDir + os.sep + 'Texte-ALG-TEI/' + text + '*.xml'):
                shutil.copy (fl, dr)
            changeVariantFilenames (dr)
            if cont:
                try:
                    os.remove (os.path.join (dr, 'overall.bt'))
                    for lang in getNewLangs (dr, texts[text]):
                        print 'h2u:', comm4 % (dr, lang)
                        os.system (comm4 % (dr, lang))
                        os.move (os.path.join (dr, 'overall.bt'), os.path.join (dr, 'overall_%s.bt' % lang))
                    with open (os.path.join (dr, 'overall.bt'), 'w') as outF:
                        for fname in [fl for fl in os.listdir (dr) if f.startswith ('overall_')]:
                            with open (fname) as inF:
                                outF.write (inF.read())
                except OSError:
                    pass
            else:
                print 'h2u:', comm4 % (dr, '-nopivotlng')
                os.system (comm4 % (dr, '-nopivotlng'))
        os.chdir (workingDir)
    print 'Preparing done'

def godHelpUs (texts, args):

    # stage 3 - uplug (word alignment)

    print 'Stage 3: making word alignment'
    #print 'Skipping... (in this version of the script)'
    #return
    print """Note: word alignment requires lot of time and resources (CPU, RAM, disc space).
Do you want to proceed? If you choose "Skip" option corpus will be only sentence-aligned"""
    answer = 'abulafia'
    # while answer not in ['p', 'c', 's']:
        # print '(P)roceed  (C)ancel  (S)kip'
        # answer = raw_input ().lower ()
    # if answer != 'p':
        # return answer
    cur_path = os.path.abspath ('.')
    os.chdir (SCRIPT_PATH)
    command = 'python UplugBatch.py'
    act_texts = texts
    if args.uplug_path:
        command += ' -u ' + args.uplug_path
    elif UPLUG_PATH:
        command += ' -u ' + UPLUG_PATH
    if args.uplug_email:
        command += ' -e ' + args.uplug_email
    if args.uplug_qsub != None:
        command += ' --qsub ' + args.uplug_qsub
    if args.concatenate:
        command += ' --concatenate'
        act_texts = ['All']
    if args.uplug_no_run:
        command += ' --no-run'
    for text in act_texts:
        text = 'Align_' + text
        print 'Uplug: ', command + ' ' + os.path.join (TEMP_PATH, text)
        os.system (command + ' ' + os.path.join (TEMP_PATH, text))
    os.chdir (cur_path)

def makeFullAlignment (texts, onlySent = False):

    # stage 4 - convert linked files to aligned XML

    print 'Stage 4: making aligned XML-files'
    curdir = os.path.abspath ('.')
    os.chdir (SCRIPT_PATH)
    link2word = 'Link2WordAlignedSentAlignedXML.xslt'
    comm1 = 'perl AdjustLinkFiles.pl %s'
    comm2 = 'perl generateAllBashFilesMW.pl %s "%s" "%s" "%s" "%s"'
    comm2 = JAVA_COMM + ' Link2WordAlignedSentAlignedXML.xslt Link2WordAlignedSentAlignedXML.xslt timestamp="no" explicitename="yes" outputdir="%s" paralleltext="%s" lngs="%s" pathToTEICorpus="%s" pathToTEIAlign="%s" onlySent="%s" absolutePath="%s"'
    comm3 = 'sh %s-cmbn.sh'
    pathAlign = ''
    pathRPC = os.path.join (curdir, 'Texte-rpc-TEI')
    outputdir = os.path.join (curdir, 'FullAlignment')
    if onlySent:
        pathAlign = os.path.join (curdir, 'Texte-ALG-TEI')
    try:
        os.mkdir (os.path.join (curdir, 'FullAlignment'))
    except OSError:
        pass
    for text, langs in texts.items ():
        try:
            os.mkdir (text)
        except OSError:
            pass
        for fl in glob.glob ('Align_' + text + '/*/*.links'):
            shutil.move (fl, text)
        if not onlySent:
            print comm1 % text
            os.system (comm1 % text)
        for i in range (len (langs)):
            ln = ' '.join ([langs[i]] + [langs[j] for j in range (len (langs)) if i != j])
            print comm2 % (outputdir, text, ln, pathRPC, pathAlign, onlySent and 'yes' or 'no', os.path.isabs (curdir) and 'yes' or 'no')
            os.system (comm2 % (outputdir, text, ln, pathRPC, pathAlign, onlySent and 'yes' or 'no', os.path.isabs (curdir) and 'yes' or 'no'))
        #print comm3 % text
        #os.system (comm3 % text)
    os.chdir (curdir)
    print 'Stage 4 completed.'

def makeCWBFiles ():

    # stage 5 - make XML files CWB-compliant

    global JAVA_COMM
    global CWB_PATH
    global SCRIPT_PATH

    curdir = os.path.abspath ('.')
    os.chdir (SCRIPT_PATH)
    try:
        os.mkdir (os.path.join (curdir, 'CORPUS'))
    except OSError:
        pass
    try:
        os.mkdir (os.path.join (curdir, 'CWBFiles'))
    except OSError:
        pass
    comm1 = JAVA_COMM + ' %s GeneralizedLink2CWB.xslt -o:%s rawpath="%s" cwbutilspath="%s"' % (os.path.join (curdir, 'FullAlignment'), os.path.join (curdir, 'CWBFiles'), os.path.join (curdir, 'CORPUS/'), CWB_PATH)
    comm2 = 'perl AdjustCWBFiles.pl %s' % os.path.join (curdir, 'CWBFiles')
    print 'Stage 5: generating CWB files...'
    print comm1
    os.system (comm1)
    print comm2
    os.system (comm2)
    os.chdir (curdir)
    print 'Stage 5 completed.'

def lastJump (metaFields, mergeName = ''):

    # stage 6 - make corpus out of CWB files

    curdir = os.path.abspath ('.')
    os.chdir (os.path.join (TEMP_PATH, 'CWBFiles'))
    print 'Stage 6: making corpus (finally!)'
    try:
        os.makedirs ('../CORPUS/Data')
        os.makedirs ('../CORPUS/Registry')
    except OSError:
        pass
    if metaFields:
        meta = addMeta (metaFields, RPC_PATH, os.path.abspath ('.'))
    else:
        meta = ''
    print '!! Meta:', meta
    if mergeName:
        mergeTexts (os.path.abspath ('.'), mergeName, meta = meta)
    #newEncode ('/data/largedata/Genewa/Diego/TEMP_CORPUS/CWBFiles/encode.pl', ['EN', 'ES', 'FR'], 'diego', '-S meta:0++fileName+Year+Country+Language')
    os.system ('perl encode.pl')
    os.chdir (curdir)

    print 'Stage 6 completed'


def clean ():

    # clean at the end

    global DESTINATION

    os.chdir (os.path.join (TEMP_DIR, TEMP_NAME))
    print 'clean:', os.path.abspath ('.')
    while True:
        try:
            shutil.copytree ('CORPUS', DESTINATION)
            break
        except OSError:
            print 'Directory %s already exists. If you want, you can specify another directory:' % os.path.join (DESTINATION, 'CORPUS')
            DESTINATION = raw_input ('Try once more (ENTER to cancel): ')
            if not DESTINATION:
                break
    print "Do you want to remove temporary files? Don't do it if you are not ABSOLUTELY sure everything's ok (you may remove TEMP_CORPUS directory by yourself later)"
    answer = 'abulafia'
    while answer not in ['y', 'n', 'yes', 'no']:
        print '(y)es / (n)o'
        answer = raw_input ().lower ()
    if answer.startswith ('y'):
        os.chdir (TEMP_DIR)
        shutil.rmtree (TEMP_NAME)

def addMeta (fields, rpcDir, cwbDir):

    for fcwb in os.listdir (cwbDir):
        tname = os.path.splitext (fcwb)[0]
        frpc = os.path.join (rpcDir, tname + '.rpc')
        if not os.path.exists (frpc):
            continue
        with open (frpc) as fin:
            rpcText = fin.read ()
        meta = []
        for field in fields:
            match = re.search (r'<%s>([^<]*)</%s>' % (field, field), rpcText)
            if match:
                meta.append ('%s="%s"' % (field, match.group (1)))
            elif field.lower () == 'filename':
                meta.append ('%s="%s"' % (field, tname))
        with open (os.path.join (cwbDir, fcwb)) as fcwbin:
            cwbText = fcwbin.read ()
        with open (os.path.join (cwbDir, fcwb), 'w') as fcwbout:
            fcwbout.write ('<meta ' + ' '.join (meta) + '>\n')
            fcwbout.write (cwbText)
            fcwbout.write ('\n</meta>')
    with open (os.path.join (cwbDir, 'encode.pl')) as fenc:
        lines = fenc.readlines ()
    for i in range (len (lines)):
        if 'cwb-encode ' in lines[i]:
            pos = lines[i].find (' ");')
            if pos == -1:
                continue
            lines[i] = lines[i][:pos] + ' -S meta:0+' + '+'.join (fields) + lines[i][pos:]
    with open (os.path.join (cwbDir, 'encode.pl'), 'w') as fenc:
        for line in lines:
            fenc.write (line)
    return ' -S meta:0+' + '+'.join (fields)

def newEncode (fname, langs, textName, meta = ''):

    data = os.path.join (TEMP_DIR, TEMP_NAME, 'CORPUS', 'Data', (textName + '_%s'))
    reg = os.path.join (TEMP_DIR, TEMP_NAME, 'CORPUS', 'Registry')
    lines = [
                'system("mkdir %s");\n' % data,
                'system("%s -d %s -f ./%s.CWB -R %s -c utf8 -xsB -P lemma -P tag -P id %s -S s:0+id %s");\n' % (os.path.join (CWB_PATH, 'cwb-encode'), data, '%s', os.path.join (reg, '%s') , '%s', '%s'),
                'system("%s -r %s %s");\n' % (os.path.join (CWB_PATH, 'cwb-makeall'), reg, '%s'),
                'open (OUT, ">>%s");print OUT "\\nALIGNED %s\\n";close (OUT);\n' % (os.path.join (reg, '%s'), '%s'),
                'system("%s -r %s -S Align_%s -o out.align %s Align_%s");\n' % (os.path.join (CWB_PATH, 'cwb-align'), reg, '%s', '%s', '%s'),
                'system("%s -r %s -D out.align");\n' % (os.path.join (CWB_PATH, 'cwb-align-encode'), reg)
            ]
    with open (fname, 'w') as fenc:
        for lang in langs:
            aligned = [l for l in langs if l != lang]
            fenc.write (lines[0] % lang.lower ())
            structAl = ' '.join (['-S Align_%s_%s' % tuple (sorted ([lang.upper (), alg.upper ()])) for alg in reversed (aligned)])
            fenc.write (lines[1] % (lang.lower (), (textName + '_' + lang).upper (), (textName + '_' + lang).lower (), structAl, meta))
            fenc.write (lines[2] % (textName + '_' + lang).upper ())
            for alg in reversed (aligned):
                fenc.write (lines[3] % ((textName + '_' + lang).lower (), (textName + '_' + alg).lower ()))
        for lang in langs:
            aligned = [l for l in langs if l != lang]
            for alg in reversed (aligned):
                first, second = sorted ([lang, alg])
                fenc.write (lines[4] % ((first + '_' + second).upper (), (textName + '_' + lang + ' ' + textName + '_' + alg).upper (), (first + '_' + second).upper ()))
                fenc.write (lines[5])



def mergeTexts (cwbDir, mergeName, langsToDo = [], meta = ''):

    langs = set ()
    print 'merge'
    print cwbDir
    files = {}
    pat = re.compile (r'^\s*<Align_([^_]+_[^>]+)')
    for fname in os.listdir (cwbDir):
        try:
            name, lang = os.path.splitext (fname)[0].split ('_')
        except:
            continue
        langs.add (lang)
        if name in files:
            files[name].append (lang)
        else:
            files[name] = [lang]
    uncomplete = {name: value for name, value in files.items () if len (value) < len (langs)}
    print 'unc:', uncomplete
    for name, lngs in uncomplete.items ():
        rest = list (langs.difference (lngs))
        print 'rest:', rest
        llens = {}
        for l in lngs:
            llen = 0
            with open (os.path.join (cwbDir, (name + '_' + l + '.cwb').upper ())) as fin:
                new = []
                for line in fin:
                    if line.startswith ('<s id'):
                        llen += 1
                        for r in rest:
                            first, second = sorted ([l, r])
                            new.append ('<Align_%s_%s>\n' % (first, second))
                            new.append (line)
                    elif (line.startswith ('</s>')):
                        new.append (line)
                        for r in rest:
                            first, second = sorted ([l, r])
                            new.append ('</Align_%s_%s>\n' % (first, second))
                    else:
                        new.append (line)
            llens[l] = llen
            with open (os.path.join (cwbDir, (name + '_' + l + '.cwb').upper ()), 'w') as fout:
                fout.write (''.join (new))
        print 'llens:', llens
        for r in rest:
            with open (os.path.join (cwbDir, (name + '_' + r + '.cwb').upper ()), 'w') as fout:
                for i in range (max ([el[1] for el in llens.items ()])):
                    for a in lngs:
                        if i < llens[a]:
                            first, second = sorted ([a, r])
                            fout.write ('<Align_%s_%s>\n' % (first, second))
                    fout.write ('--\n')
                    for a in lngs:
                        if i < llens[a]:
                            first, second = sorted ([a, r])
                            fout.write ('</Align_%s_%s>\n' % (first, second))

    langs = list (langs)
    for name in files.keys ():
        for lang in langs:
            fname = (name + '_' + lang + '.cwb').upper ()
            print fname
            if langsToDo and not lang in langsToDo:
                continue
            with open (os.path.join (cwbDir, (mergeName + '_' + lang + '.cwb').upper ()), 'a') as fbig:
                try:
                    with open (os.path.join (cwbDir, fname)) as fsmall:
                        fbig.write (fsmall.read ().replace ('<?xml version="1.0" encoding="UTF-8"?>\n', '') + '\n')
                    #os.remove (os.path.join (cwbDir, fname))
                except IOError:
                    dummy = ''
                    aligned = [(pair, count) for pair, count in files[name].items () if lang.upper () in pair]
                    for i in range (max ([el[1] for el in aligned])):
                        for pair, count in aligned:
                            if i < count:
                                fbig.write ('<Align_%s>\n' % pair)
                        fbig.write ('--\n')
                        for pair, count in aligned:
                            if i < count:
                                fbig.write ('</Align_%s>\n' % pair)
    newEncode (os.path.join (cwbDir, 'encode.pl'), langs, mergeName, meta)


if __name__ == '__main__':
    parser = argparse.ArgumentParser ()
    parser.add_argument ('-v', '--verbose', action = 'store_true')
    parser.add_argument ('-l', '--languages', nargs = '+')
    parser.add_argument ('-s', '--start-stage', dest = 'start', type = int, default = 1)
    parser.add_argument ('-e', '--end-stage', dest = 'end', type = int, default = 6)
    parser.add_argument ('-a', '--add-texts', nargs = '+', dest = 'addTexts')
    parser.add_argument ('-H', '--hunalign')
    parser.add_argument ('-S', '--path-to-scripts', dest = 'scripts')
    parser.add_argument ('-C', '--path-to-cwb', dest = 'cwb')
    parser.add_argument ('-d', '--destination')
    parser.add_argument ('-i', '--input')
    parser.add_argument ('-t', '--tmp-dir', dest = 'temp')
    parser.add_argument ('-oS', '--only-sent', action = 'store_true')
    parser.add_argument ('-m', '--add-meta', nargs = '+')
    parser.add_argument ('-M', '--merge-texts')
    parser.add_argument ('-uP', '--uplug-path')
    parser.add_argument ('-uQ', '--uplug-qsub', nargs = '?', const = '')
    parser.add_argument ('-uE', '--uplug-email')
    parser.add_argument ('-uNR', '--uplug-no-run', action = 'store_true')
    parser.add_argument ('-c', '--concatenate', action = 'store_true')
    args = parser.parse_args ()
    if args.hunalign:
        HUN_PATH = args.hunalign
    if args.scripts:
        SCRIPT_PATH = args.scripts
    if args.cwb:
        CWB_PATH = args.cwb
    if args.destination:
        DESTINATION = args.destination
    if args.input:
        RPC_PATH = args.input
    if args.temp:
        TEMP_DIR = args.temp
    TEMP_PATH = os.path.abspath (os.path.join (TEMP_DIR, TEMP_NAME))
    if not os.path.isabs (HUN_PATH):
        HUN_PATH = os.path.abspath (HUN_PATH)
    if not os.path.isabs (SCRIPT_PATH):
        SCRIPT_PATH = os.path.abspath (SCRIPT_PATH) 
    if not os.path.isabs (UPLUG_PATH):
        UPLUG_PATH = os.path.abspath (UPLUG_PATH)       
    if not os.path.isabs (CWB_PATH):
        CWB_PATH = os.path.abspath (CWB_PATH)       
    if not os.path.isabs (RPC_PATH):
        RPC_PATH = os.path.abspath (RPC_PATH)       
    stages = range (args.start, args.end + 1)
    texts = makePreparations (stages, args)

    print "Let's go!"
    if 1 in stages:
        hunThemAll ()
    if 2 in stages:
        hun2uplug (texts, concatenate = args.concatenate)
    if 3 in stages and not args.only_sent:
        godHelpUs (texts, args)
    if 4 in stages:
        makeFullAlignment (texts, args.only_sent)
    if 5 in stages:
        makeCWBFiles ()
    if 6 in stages:
        lastJump (args.add_meta, args.merge_texts)
    #   clean ()
