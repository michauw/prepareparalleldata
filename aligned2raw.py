# Split combined aligned documents (in LF-Aligner-like format) into separate one-language files
# v 1.0
# Last modification: 10.09.2019
# michauwww@gmail.com

import os
import argparse

def get_files (path, recursive, extensions = 'all'):
    if os.path.isfile (path):
        return [path]
    if recursive:
        filelist = [os.path.join (root, fname) for root, dirs, files in os.walk (path) for fname in files]
    else:
        filelist = [f for f in glob.glob (os.path.join (path, '*')) if os.path.isfile (f)]
    if extensions != 'all':
        filelist = [f for f in filelist if os.path.splitext (f)[1] in extensions]
    
    return filelist
        
def make_output_list (filelist, basepath, outpath, remove = []):
    outfiles = []
    if not basepath.endswith (os.sep):
        basepath += os.sep
    if not outpath.endswith (os.sep):
        outpath += os.sep
    for f in filelist:
        dir, fname = os.path.split (f)
        dir = dir.replace (basepath, outpath)
        if not os.path.exists (dir):
            os.makedirs (dir)
        for r in remove:
            fname = fname.replace (r, '')
        name, ext = os.path.splitext (fname)
        fname = name + '%s' + ext
        outfiles.append (os.path.join (dir, fname))
    
    return outfiles
    
def make_raw (path, langs, sep = '\t'):
    
    texts = {lang: [] for lang in langs}
    with open (path) as fin:
        for line in fin:
            segments = line.strip ('\n').split (sep)
            for lang, segment in zip (langs, segments):
                texts[lang].append (segment)
    
    return texts

if __name__ == '__main__':
    parser = argparse.ArgumentParser ()
    parser.add_argument ('input')
    parser.add_argument ('-l', '--languages', nargs = '+')
    parser.add_argument ('-e', '--extensions', nargs = '+', default = ['.txt'])
    parser.add_argument ('-r', '--recursive', action = 'store_true')
    parser.add_argument ('-o', '--output')
    parser.add_argument ('-s', '--separator', default = '\t')
    parser.add_argument ('-d', '--delete', nargs = '+', default = [])
    
    args = parser.parse_args ()
    
    filelist = get_files (args.input, args.recursive, args.extensions)
    outputlist = make_output_list (filelist, args.input, args.output, remove = args.delete)
    
    for inp, outp in zip (filelist, outputlist):
        texts = make_raw (inp, args.languages, args.separator)
        for lang, text in texts.items ():
            with open (outp % ('_' + lang), 'w') as fout:
                fout.write ('\n'.join (text))