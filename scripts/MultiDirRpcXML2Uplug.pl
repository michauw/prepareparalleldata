use File::Spec::Functions;
use File::Copy;
use Cwd;
use open ':utf8';
#zun�chst Argument parsen
#alle Argumente in einen Hash, dann sind sie leicht abzufragen
for $par (@ARGV){
	#parameter�bergabe?
	if ($par  =~ /^(.+)=(.+)$/){
		$args{$1}=$2;
	}
	else {#Schalter bzw. Dateiname
		$args{$par}=1;
	}
}

print "Nimmt ein Verzeichnis mit TEI-enkodierten XML Dateien und TEI-kodierten ALIGNdateien ";
print "und generiert in diesem Verzeichnis die notwendigen Unterverzeichnisse und eine BT-Datei für Uplug";
print "Argumente: Directory [picotlng] -nopivotlng\n";




$dir = $ARGV[0];
print "\nMulti dir: $dir\n";
(opendir DIR, $dir) || die("Could not open directory $dir\n"); 
		@all_files = grep /^(\S+)\_(\S\S\S?).rpc.xml$/, readdir DIR;
closedir(DIR);

$pivotlng="pl";
$pivotlng = $ARGV[1] if (defined $ARGV[1]);

print "using all LNGS!\n" if  ( $args{"-nopivotlng"});

for (@all_files) {
	$_=~/^(\S+)\_(\S\S\S?).rpc.xml$/; 
	$lng{$2}++; 
	$name{$1}++;
}


@languages= (sort (keys %lng));
@file_name_bodies=(sort(keys %name));
$odir="$dir";
system ("mkdir $odir");
for $lngNr (0..@languages-1){
	for $lngNr2 ($lngNr+1..@languages-1){
		print "$languages[$lngNr]\t $languages[$lngNr2]\n";
		for  $fileBody(@file_name_bodies){
			$currFile1="$dir/$fileBody\_".$languages[$lngNr].".rpc.xml"; 
			$currFile2="$dir/$fileBody\_".$languages[$lngNr2].".rpc.xml"; 
			$lngName1= lc( $languages[$lngNr]);
			$lngName2= lc ($languages[$lngNr2]);
			$linkfile="$lngName1-$lngName2.xml"; 
			$linkfileOrig="$dir/$fileBody\_".$languages[$lngNr]."-".$languages[$lngNr2].".alg.xml";
			if (($lngName1 =~ /$pivotlng/) or ($lngName2 =~ /$pivotlng/) or $args{"-nopivotlng"}){  
			print "copying $fileBody, $currFile1, $currFile2\n";  
			system ("mkdir $odir/$lngName1-$lngName2");
			system ("mkdir $odir/$lngName1-$lngName2/$lngName1");
			system ("mkdir $odir/$lngName1-$lngName2/$lngName2");
			system ("cp $currFile1 $odir/$lngName1-$lngName2/$lngName1/$fileBody.xml");
			system ("cp $currFile2 $odir/$lngName1-$lngName2/$lngName2/$fileBody.xml");
			system ("cp $linkfileOrig $odir/$lngName1-$lngName2/$linkfile");
			
			$batchfile="$odir/$lngName1-$lngName2/$lngName1-$lngName2.sh";
			open (BATCH, ">$batchfile") or die ("couldn't open batch file");
			print BATCH "~/uplug/uplug align/word/tagged -in $lngName1-$lngName2.xml -out $lngName1-$lngName2.links";
			close BATCH; 
			$allbatch.="cd ../"; 
			$allbatch.="$lngName1-$lngName2\n"; 
			$allbatch.="qsub -M waldenfels@issl.unibe.ch -cwd -l h_cpu=96:00:00,h_vmem=6g $lngName1-$lngName2.sh\n"; 

			}
		}
	}
}

open (ALLBATCH, ">$odir/overall.bt"); 
print ALLBATCH $allbatch;


