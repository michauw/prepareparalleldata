import os, sys, argparse

UPLUG_PATH = '~/uplug/uplug'

parser = argparse.ArgumentParser ()
parser.add_argument ('-q', '--qsub', nargs = '?', const = 'qsub -V -d .')
parser.add_argument ('-e', '--email')
parser.add_argument ('-u', '--uplug-path')
parser.add_argument ('-c', '--concatenate', action = 'store_true')
parser.add_argument ('-nr', '--no-run', action = 'store_true')
parser.add_argument ('directory')

sargs = sys.argv[1:]
sargs.insert (0, sargs[-1])
sargs.pop ()
args = parser.parse_args (sargs)

dir = args.directory
final = r' \1'
if args.concatenate:
	final = r' %s'
if args.uplug_path:
	UPLUG_PATH = args.uplug_path
command = r'sh' + final
if args.qsub:
	command = args.qsub
	if args.email:
		command += ' -m e -M %s' % args.email
	command += final

xmldirs = set ()
for root, dirs, files in os.walk (dir):
	for f in files:
		if os.path.splitext (f)[1] == '.sh':
			curdir = os.path.split (root)[1]
			if curdir.endswith ('/'):
				curdir = curdir[:-1]
			xmldirs.add (curdir)
			print os.path.join (root, f)
			with open (os.path.join (root, f)) as fin:
				text = fin.read ()
			text = text.replace ('~/uplug/uplug ', UPLUG_PATH + ' ')
			with open (os.path.join (root, f), 'w') as fout:
				fout.write (text)
xmldirs = sorted (xmldirs)
if args.concatenate:
	with open (os.path.join (dir, 'overall.sh'), 'w') as fsh:
		fsh.write ('cd %s\n' % xmldirs[0])
		fsh.write ('echo %s\n' % xmldirs[0])
		fsh.write (command % (xmldirs[0] + '.sh\n'))
		for dr in xmldirs[1:]:
			fsh.write ('cd ../%s\n' % dr)
			fsh.write ('echo %s\n' % dr)
			fsh.write (command % (dr + '.sh\n'))
else:
	os.system ("sed -i -r '0,/cd ..\//s//cd /' " + os.path.join (dir, 'overall.bt'))
	os.system (("sed -i -r 's/qsub -M waldenfels.unibe.ch -cwd -l h_cpu=96:00:00,h_vmem=6g (.*.sh)/%s/g' " % command) + os.path.join (dir, 'overall.bt'))
	os.system ('mv %s %s' % (os.path.join (dir, 'overall.bt'), os.path.join (dir, 'overall.sh')))
os.chdir (dir)
if not args.no_run:
	os.system ('sh overall.sh')