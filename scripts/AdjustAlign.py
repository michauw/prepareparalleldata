import sys, os, re

for root, dirs, names in os.walk (sys.argv[1]):
	for name in names:
		rname = re.search (r'^(.*)_(\w+)-(\w+).alg$', name)
		if rname:
			nm, fromf, tof = rname.groups()
			with open (os.path.join (root, name)) as i:
				content = i.read()
			if '<ALIG fromFile' in content[:100]:
				continue
			with open (os.path.join (root, name), 'w') as o:
				o.write ('<ALIG fromFile="%s/%s.xml" toFile="%s/%s.xml">\n' % (fromf.lower(), nm, tof.lower(), nm))
				o.write (content)
				o.write ('</ALIG>\n')
