use RPC_XML_Tags;
#binmode(STDOUT, ":cp1252");

use File::Spec::Functions;
use Cwd;
use open ':utf8';

#zun�chst Argument parsen
#alle Argumente in einen Hash, dann sind sie leicht abzufragen
for $par (@ARGV){
	#parameter�bergabe?
	if ($par  =~ /^(.+)=(.+)$/){
		$args{$1}=$2;
	}
	else {#Schalter bzw. Dateiname
		$args{$par}=1;
	}
}


#Eingabeverzeichnis, falls undefiniert: .
if (defined $args{'-idir'}){
	$idir=$args{'-idir'};
}
else {
	$idir=".";
}

#Ausgabeverzeichnis, falls undefiniert: .
if (defined $args{'-odir'}){
	$odir=$args{'-odir'};
}
else {
	$odir="ARBEIT";
}

#Batchmode aktivieren
if (defined $args{'-batch'}){
	$batchmode=1;
}

#lemmamode aktivieren
if (defined $args{'-lemma'}){
	$lemmaMode=1;
}



if ($args{-silent}==0){
  print "Converts XML-files into files fit for alignment by hunalign. New name is RUMP_LGN.snt. \n";
  print "-lemma writes out the lemmas instead of tokens. \n";
  print "-idir=input direcory (default is working directory)\n";
  print "-odir=output direcory (default is ARBEIT).\n";
  print "Directories will be created if they don't exist. \n";
  print "-batch mode will process all rpc files in the input directory\n";
  print "Argumente: [einzelneDatei] [-batch] [-idir=VERZ] [-odir=VERZ] [-lemma] [-silent]\n";
}


#variablen, XML-Strings
$begS="$TGbegSStart[0-9]+$TGbegSEnd";
$endS=$TGendS;



sub stripXML(){

	if ($lemmaMode && $isLemmatized){
		#nimm das lemma
		$_[0] =~ s/$TGTokenStartRump.*?$TGLemmaStart(.*?)$TGLemmaEnd.*?$TGendToken/ $1 /g;
		}
	else{
		#nimm das wort
		$_[0] =~ s/$TGTokenStartRump.*?>([^<]+).*?$TGendToken/ $1 /g;
	}
	#replace <div endings
	$_[0] =~ s/<\/div>/---!---DIV---!----/g;
	#dann rausnehmen alle tags
	$_[0] =~ s/<.*?>/ /g;
	#reinsert <p endings
	$_[0] =~ s/---!---DIV---!----/<p>/g;

	#dann rausnehmen alle spaces
	$_[0] =~ s/ +/ /g;
	#dann rausnehmen alle anf�nglichen spaces
	$_[0] =~ s/^ //g;
	#take out double \n's
	$_[0] =~ s/\n+ /\n/g;
	#dann Strings die nur leerzeichen enthalten ganz l�schen (inclusive Zeilenwechsel)
	if ($_[0] =~ /^\s*$/)
		{$_[0]="";}

	return $_[0];
}


#AB HIER DAS EIGENTLICHE SCRIPT

if ($batchmode){
	#alle rpcs im Dir
	(opendir DIR, $idir) || die("Could not open directory $idir\n");
	@all_rpc_files = grep /\.rpc$/, readdir DIR;
	closedir(DIR);
}
else{
	@all_rpc_files[0]=$ARGV[0];
}


foreach $file_name (@all_rpc_files) {
	print "doing $file_name...\n";
	#falls lemmatisierte Daten verwendet werden sollen,
	#erst mal checken ob es sich um lemmatisierten text handelt	
	if ($lemmaMode){
		print "Supposedly lemmatized version. Checking ... ";
		
		open (EINGABE, catfile ($idir, $file_name)) or die " $file_name konnte nicht zum lesen ge�ffnet werdeb!";
		$zeile="";
		#vorgehen zur ersten Zeile mit Wort
		while (not $zeile=~ /$TGTokenStartRump/) {
			$zeile=<EINGABE>;
		}
		#auch Lemmatag da? Dann isLemmatized setzen, sonst nicht
		if ($zeile=~/$TGLemmaStart/){
			$isLemmatized=1;
			print "lemma tag found!\n";
		}
		else{
			$isLemmatized=0;
			print " failure to fing lemma tag, no lemmatization presen!!\n";
		}
		close (EINGABE);
	
	}
	
	#OK, jetzt vom Anfang
	open (EINGABE, catfile ($idir, $file_name)) or die " $file_name konnte nicht zum lesen ge�ffnet werdeb!";
	
	
	(opendir (ODIR, $odir)) || (mkdir ($odir)); #erzeugt Verzeichnis, falls nicht vorhanden
	(opendir (ODIR, $odir)); #if only created
	
	
	#Dateinamen aufteilen in Rumpf und Sprache mit Endung
	$newfilename=$file_name;
	
	#languageencoding
	$newfilename =~ /\_(\w\w\w?).rpc/;
	$LNG= $1;
	print "language is $LNG!\n";
	$enc=TGgetWinEncoding('');
	
	$newfilename=~ s/rpc$/snt/; #versieht die Datei mit einer neuen Endung
	$newfilename=catfile ($odir, $newfilename); #Dateinamen mit Pfad zusammenf�hren
	next if (defined ($args{"-c"}) and -f $newfilename);
	
	open(OUTPUT, ">$enc", "$newfilename") or die "$newfilename nicht ge�ffnet!";
	
	$line="";
	#bis zum eigentlichen Header vorgehen, der an einer Leerzeile sein muss
	until ($line =~ /$TGbegBody/){
		$line =<EINGABE>;
	}
	
	#jetzt weiter bis zum Ende des Textes
	for    $line (<EINGABE>){ #neue Zeile laden
		print OUTPUT &stripXML($line);#die kurze Version rausschreiben
	}
	print "$file_name: $newfilename written.\n";
	close OUTPUT;
}


#if write batch file...
#(opendir DIR, $odir) || die("Could not open directory $dir\n");
#@all_rpc_files = grep /\.snt$/, readdir DIR;
#closedir(DIR);


	



#fertig