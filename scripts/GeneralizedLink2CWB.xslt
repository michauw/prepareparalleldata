<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions" >
	<xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/> 
	<!-- <xsl:preserve-space elements="*"/> -->
	<xsl:param name="verbose">no</xsl:param>
	<xsl:param name="rawpath">/gpfs/homefs/issl/waldenfels/WordAlignedExperiments/THISNEWCORPUS/CORPFILES/</xsl:param>
	<xsl:param name="regpath"><xsl:value-of select="concat($rawpath, 'Registry')"/></xsl:param>
	<xsl:param name="datapath"><xsl:value-of select="concat($rawpath, 'Data')"/></xsl:param>
	<xsl:param name="cwbutilspath" select="'/gpfs/homefs/issl/waldenfels/CWBNEU/cwb/utils'"/>

	
	
	
	<xsl:template match="/">
<xsl:message><xsl:value-of select="$cwbutilspath"/></xsl:message>
<xsl:variable name="mainlng">
<xsl:value-of select="upper-case(replace(/corpus/@lngs, '^(\w+)::.*', '$1'))"/>
</xsl:variable>

<xsl:variable name="paralleltext">
<xsl:value-of select="/corpus/@text"/>
</xsl:variable>



<xsl:result-document href="{concat ('./', $paralleltext,'_', $mainlng, '.CWB')}" method="xml" encoding="utf8">

<xsl:value-of select="'&#10;'"/>
<xsl:comment>
<xsl:value-of select="'&#10;'"/>
<xsl:value-of select="concat('======1','system(&quot;mkdir ', $datapath, '/',lower-case(concat($paralleltext, '_',$mainlng)), '&quot;);&#10;')"/>
<xsl:value-of select="'======1system(&quot;'"/>
<xsl:value-of select="concat('',$cwbutilspath, '/cwb-encode -d ', $datapath, '/', lower-case(concat($paralleltext, '_',$mainlng)), ' -f ',concat ('./', $paralleltext,'_', $mainlng, '.CWB'),' -R ', $regpath, '/',lower-case(concat($paralleltext, '_',$mainlng)),' ')"/>
<xsl:value-of select="'-c utf8 -xsB -P lemma -P tag -P id '"/>
<xsl:for-each select="//s[1]/w[1]/waligns/walign">
<xsl:value-of select="concat ('-P ', @lng, '/ ')"/>
</xsl:for-each>
<xsl:for-each select="distinct-values(//s[10]/preceding-sibling::BegAlign/@lng)">
<xsl:variable name="alignedlng">
<xsl:analyze-string select="." regex="\w+">
<xsl:matching-substring><xsl:element name="lng"><xsl:value-of select="."/></xsl:element></xsl:matching-substring>
</xsl:analyze-string>
</xsl:variable>
<xsl:value-of select="concat ('-S Align', '')"/>
<xsl:for-each select="$alignedlng/lng">
<xsl:sort select="."/>
<xsl:value-of select="concat ('_', .)"/>
</xsl:for-each>
<xsl:value-of select="' '"/>
</xsl:for-each>
<xsl:value-of select="'-S s:0+id '"/>
<xsl:value-of select="'&quot;);&#10;'"/>
<xsl:value-of select="'======1system(&quot;'"/>
<xsl:value-of select="concat('',$cwbutilspath, '/cwb-makeall -r ',$regpath, ' ', upper-case(concat($paralleltext, '_',$mainlng)))"/>
<xsl:value-of select="'&quot;);&#10;'"/>




<xsl:for-each select="distinct-values(//s[10]/preceding-sibling::BegAlign/@lng)">
<xsl:variable name="alignedlng">
<xsl:analyze-string select="." regex="\w+">
<xsl:matching-substring><xsl:element name="lng"><xsl:value-of select="."/></xsl:element></xsl:matching-substring>
</xsl:analyze-string>
</xsl:variable>
<xsl:variable name="tag">
<xsl:value-of select="concat ('Align', '')"/>
<xsl:for-each select="$alignedlng/lng">
<xsl:sort select="."/>
<xsl:value-of select="concat ('_', .)"/>
</xsl:for-each>
</xsl:variable>

<xsl:variable name="source" select="$alignedlng/lng[1]"/>
<xsl:variable name="target" select="$alignedlng/lng[2]"/>

<xsl:value-of select="concat('======1','open (OUT, &quot;>>',$regpath, lower-case(concat('/', $paralleltext, '_', $source, '&quot;);')), 'print OUT &quot;\nALIGNED ', lower-case(concat($paralleltext, '_', $target, '\n&quot;;')), 'close (OUT);')"/>
<xsl:value-of select="'&#10;'"/>
<xsl:value-of select="'======2system(&quot;'"/>
<xsl:value-of select="concat('',$cwbutilspath, '/cwb-align -r ',$regpath, ' -S ', $tag, ' -o out.align ', upper-case(concat($paralleltext, '_', $source, ' ', $paralleltext, '_', $target, ' ')),  $tag)"/>
<xsl:value-of select="'&quot;);&#10;'"/>
<xsl:value-of select="'======2system(&quot;'"/>
<xsl:value-of select="concat('',$cwbutilspath, '/cwb-align-encode -r ',$regpath, ' -D out.align','')"/>
<xsl:value-of select="'&quot;);&#10;'"/>


</xsl:for-each>




</xsl:comment>
<xsl:value-of select="'&#10;'"/>
<xsl:apply-templates/>
</xsl:result-document>
</xsl:template>

<xsl:template match="BegAlign">
<xsl:element name="{concat('BegAlign', '_',@lng)}"/>
<xsl:value-of select="'&#10;'"/>
</xsl:template>

<xsl:template match="EndAlign">
<xsl:element name="{concat('EndAlign', '_', @lng)}"/>
<xsl:value-of select="'&#10;'"/>
</xsl:template>


<xsl:template match="s">
<xsl:element name="s">
<xsl:attribute name="id" select="@id"/>
<!--  
<xsl:value-of select="concat('&lt;s id=&quot;',@id,'&quot;&gt;','&#10;' )"/>
-->
<xsl:value-of select="'&#10;'"/>

<xsl:for-each select="w">
<xsl:value-of select="concat(if(matches(text(), '\S')) then (text()) else ('-'), '&#9;' )"/>
<xsl:value-of select="concat(if(matches(@lem, '\S')) then (@lem) else ('-'), '&#9;' )"/>
<xsl:value-of select="concat(if(matches(@pos, '\S')) then (@pos) else ('-'), '&#9;' )"/>
<xsl:value-of select="concat(if(matches(@id, '\S')) then (@id) else ('-'), '&#9;' )"/>
<xsl:for-each select="waligns/walign">
<xsl:if test="not (w)">
<xsl:value-of select="'|'"/>
</xsl:if>
<xsl:for-each select="w">
<xsl:value-of select="concat('|=w:',text(),'=i:',@id,'=l:',@lem, '=t:', @pos)"/>
<xsl:value-of select="if (position() eq last()) then ('|') else ()"/>
</xsl:for-each>
<xsl:value-of select="if (position() ne last()) then ('&#9;') else ()"/>
</xsl:for-each>
<xsl:value-of select="'&#10;'"/>
</xsl:for-each>
</xsl:element>
<xsl:value-of select="'&#10;'"/>
<!-- <xsl:value-of select="concat('&lt;/s&gt;','&#10;' )"/> -->
</xsl:template>



	

	
	
	
</xsl:stylesheet>
