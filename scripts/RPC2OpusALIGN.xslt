<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
<!-- Filter version 2.2.0_qdxml - 2010-12-19 - vjl -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>
	<!-- <xsl:strip-space elements="*"/>-->  
	<xsl:preserve-space elements="*"/>

<xsl:template match="ALIG">
<cesAlign version="1.0">
<linkGrp targType="s">
<xsl:attribute name="toDoc" select="concat('', @toFile)"/>
<xsl:attribute name="fromDoc" select="concat('', @fromFile)"/>
<xsl:apply-templates/>
</linkGrp>
</cesAlign> 
</xsl:template>


<xsl:template name="lines">
<xsl:param name="from"/>
<xsl:param name="to"/>
<!-- <xsl:value-of select="concat($from, '-', $to, '!', $from+1, '-', $to+1)"/> -->
<xsl:if test="abs($to) &gt; abs($from)">
<xsl:value-of select="$from"/>
<!-- <xsl:value-of select="'HALLA'"/> -->
</xsl:if>
<!-- another sent? -->
<xsl:if test="$to &gt; ($from+1)">
<xsl:value-of select="' '"/>
<xsl:call-template name="lines">
<xsl:with-param name="from" select="$from + 1"/>
<xsl:with-param name="to" select="$to"/>
</xsl:call-template>
</xsl:if>
</xsl:template>

<xsl:template match="alig">
<xsl:element name="link">
<xsl:attribute name="certainty" select="@type"/>
<xsl:attribute name="xtargets">
<xsl:call-template name="lines">
<xsl:with-param name="from" select="@Ln1Strt"/>
<xsl:with-param name="to" select="@Ln1End"/>
</xsl:call-template>
<xsl:value-of select="';'"/>
<xsl:call-template name="lines">
<xsl:with-param name="from" select="@Ln2Strt"/>
<xsl:with-param name="to" select="@Ln2End"/>
</xsl:call-template>
</xsl:attribute>
<xsl:attribute name="id">
<xsl:value-of select="'LN'"/>
<xsl:number/>
</xsl:attribute>
</xsl:element>
</xsl:template>

</xsl:stylesheet>
