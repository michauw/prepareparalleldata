use File::Spec::Functions;
use File::Copy;
use Cwd;
use open ':utf8';

#zunächst Argument parsen
#alle Argumente in einen Hash, dann sind sie leicht abzufragen
for $par (@ARGV){
	#parameterübergabe?
	if ($par  =~ /^(.+)=(.+)$/){
		$args{$1}=$2;
	}
	else {#Schalter bzw. Dateiname
		$args{$par}=1;
	}
}


print "Bestimmt alle möglichen Kombinationen an Sprachen und";
print "\nschreibt die entsprechenden InputDateien für die Alignierung per BSA";
print "\n in eine Verzeichnisstruktur unter BSA.";
print "-nolemma will not use lemmata for alignment\n";
print "Argumente: Directory [-nolemma] [-silent]";



$dir = $ARGV[0];
$aligDir=".";


(opendir DIR, $dir) || die("Could not open directory $dir\n"); 
		@all_rpc_files = grep /.rpc.xml$/, readdir DIR;
closedir(DIR);


#sub &copyAllFiles{
#	(opendir DIR, $_[0]) || die("Could not open directory $_[0]\n"); 
#	@all_files = grep /.+$/, readdir DIR;
#	closedir(DIR);
#	foreach (@all_files){
#	}
#}

@variants=('','a'..'z'); 

foreach $file_name (@all_rpc_files) {
		#print $file_name;
		$filenames{$file_name}++;
    $file_name =~ /(.*)_(..)(.?)\.rpc.xml$/;
		$file_name_body{$1}++;
    $language_tag{"$2$3"}++;
    $language_tag_unique{"$2"}++;
    if (not $3 eq ""){
			$language_alt_tag{"$2$3"}="$2";
		}
}

#print %language_tag;
@languages= (sort {$a cmp $b}(keys %language_tag_unique));
@languages_alt= (sort (keys %language_alt_tag));
@languages_all= (sort (keys %language_tag));
@file_name_bodies=(sort(keys %file_name_body));
print "\n".($#languages+1)." Sprachen, ".($#languages_alt +1)." Varianten gefunden, suche Kombinationen\n";
my $wd = getcwd;
print "\nWorking dir: ".$wd."\n";
open (OUTLIST, ">FILELIST.xml");

print (OUTLIST "<COMBOS>\n");

	

for $lngNr (0..@languages-1){
	for $lngNr2 ($lngNr+1..@languages-1){
			$lngName1= $languages[$lngNr];
			$lngName2= $languages[$lngNr2];
		print (OUTLIST "<COMBI lng1=\"".lc($lngName1)."\" lng2=\"".lc($lngName2)."\" >\n");
		print "$languages[$lngNr]\t $languages[$lngNr2]\n";
		for  $fileBody(@file_name_bodies){
			$lngName1= $languages[$lngNr];
			$lngName2= $languages[$lngNr2];

			for $variant1(@variants){
			for $variant2(@variants){
			$currFile1="$fileBody\_$lngName1$variant1.rpc.xml";
			$currFile2="$fileBody\_$lngName2$variant2.rpc.xml";
			$algFile="$fileBody\_$lngName1$variant1-$lngName2$variant2.alg.xml";

			if ((defined ($filenames{$currFile1})) &&(defined ($filenames{$currFile2}))){
								# first, print out the default variant
							print (OUTLIST "<COMPL file1=\"$currFile1\" file2=\"$currFile2\" alg=\"$algFile\"/>\n");
			}}
			}
		}
		print (OUTLIST "</COMBI>\n");

	}
}


# now do the language variants against each other

@variants=('','a'..'z'); 

for $lng(@languages){
# now do the language variants against each other
	for $thisvar(0..@variants-1){
			for $thisvar2($thisvar+1..@variants-1){

				print "$thisvar :". $variants[$thisvar] ."- ". $variants[$thisvar2] ."E! ";
				$lngName1="$lng".$variants[$thisvar];
				$lngName2="$lng".$variants[$thisvar2];

				if (defined ($language_tag{"$lngName2"}) and defined ($language_tag{"$lngName1"})){

					print (OUTLIST "<COMBI lng1=\"".lc($lngName1)."\" lng2=\"".lc($lngName2)."\" >\n");
					print "$lngName1\t $lngName2\n";
					for  $fileBody(@file_name_bodies){
						$currFile1="$fileBody\_$lngName1.rpc.xml";
						$currFile2="$fileBody\_$lngName2.rpc.xml";
						$algFile="$fileBody\_$lngName1-$lngName2.alg.xml";

						if ((defined ($filenames{$currFile1})) &&(defined ($filenames{$currFile2}))){
									# first, print out the default variant
									print (OUTLIST "<COMPL file1=\"$currFile1\" file2=\"$currFile2\" alg=\"$algFile\"/>\n");
						}
					}
					print (OUTLIST "</COMBI>\n");
			}
		}
	}
}
print (OUTLIST "</COMBOS>\n");

if ($variantenKopiert==1){
	print "--------------Rerunning the script for variants----------\n";
	system ("perl ".catfile (cwd(), "BuildCorpus.pl") ." $tmpDir ");
}

	print "--------------Finished preparing the corpus for alignment----------\n";
		

	
