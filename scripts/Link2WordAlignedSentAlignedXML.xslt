<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions" >
	<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>
	<!-- <xsl:strip-space elements="*"/> --> 
	<xsl:preserve-space elements="*"/>
	<xsl:param name="onlySent">no</xsl:param>
	<xsl:param name="timestamp">yes</xsl:param>
	<xsl:param name="pathToTEICorpus"></xsl:param>
	<xsl:param name="pathToTEIAlign"></xsl:param>
	<xsl:param name="explicitename">no</xsl:param>
	<xsl:param name="outputdir">CORPUS-OUTP</xsl:param>
	<xsl:param name="verbose">no</xsl:param>
	<xsl:param name="testing">no</xsl:param>
	<xsl:param name="paralleltext">BulgakovMaster</xsl:param>
	<xsl:param name="lngs">ru uk uka by pl pla cz sk sl hr sr sra mk bg</xsl:param>
	<xsl:param name="absolutePath">no</xsl:param>
	<xsl:variable name="description">
	<xsl:value-of select="'FullAlignmentXML'"/>
	</xsl:variable>

<xsl:variable name="parameter">
	<!-- <lng subsetReg="^Vmm" notLemReg="^быти|пожаловать|прощать|извинить|тереть|позволить|помиловать|изволить|просить$" primary="yes">ru</lng> -->
	<xsl:analyze-string select="$lngs" regex="(\w+)">
	<xsl:matching-substring>
	<xsl:element name="lng">
	<xsl:if test="position() eq 1">
	<xsl:attribute name="primary" select="'yes'"/>
	</xsl:if>
	<xsl:value-of select="regex-group(1)"/>
	</xsl:element>
	</xsl:matching-substring>
	</xsl:analyze-string>
	</xsl:variable>
	

	<xsl:key match="link" name="getLinks" use="fromS"/>
	<!--test="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]">-->
	<xsl:key match="links" name="getLinksbyLNG" use="@toLang"/>


	
	<xsl:key match="s" name="getTargetForms" use="@id"/>
		<!-- 
	key('getTargetForms', $toS, $forms/lang[@lng = $actLng])
	<xsl:for-each  select="$forms/lang[@lng = $actLng]/s[@id = $toS]/w[@id[. = key('getLinks', $s_id, $links/links[@toLang eq $actLng])/wordlink/fromWord[text() =$id]/toWord]]"> -->
	
	
	<xsl:template name="getForms">
	<xsl:param name="lang"/>
	<xsl:param name="posRegexp"/>
	
	<xsl:element name="lang">
	<xsl:attribute name="lng">
	<xsl:value-of select="$lang"/>
	</xsl:attribute>
	<xsl:variable name="paralleltextFileName">
	<xsl:message><xsl:value-of select="concat(if($absolutePath eq 'yes') then ('') else ('./'), if($pathToTEICorpus = '') then ($paralleltext) else ($pathToTEICorpus), '/', $paralleltext, '_', upper-case(substring($lang,1,2)), substring($lang,3,1), '.rpc.xml' )"/></xsl:message>
	<xsl:value-of select="concat(if($absolutePath eq 'yes') then ('') else ('./'), if($pathToTEICorpus = '') then ($paralleltext) else ($pathToTEICorpus), '/', $paralleltext, '_', upper-case(substring($lang,1,2)), substring($lang,3,1), '.rpc.xml' )"/>
	</xsl:variable>
	<xsl:copy-of select="doc($paralleltextFileName)//s"/>
	</xsl:element>
	<xsl:message><xsl:value-of select="concat('Read ', $paralleltext ,  ' in lang ', $lang , ' with regexp ', $posRegexp)"/></xsl:message>
	</xsl:template>
	
	<xsl:template name="getLinks">
	<xsl:param name="fromLang"/>
	<xsl:param name="toLang"/>
	<xsl:element name="links">
	<xsl:attribute name="fromLang"  select="$fromLang"/>
	<xsl:attribute name="toLang"  select="$toLang"/>
	
	<!-- wright and wrong um die verschiedenen dateinamen je nach reihenfolge zu managen -->
	<!-- paths differ in semantic makeup depending on variable pathToTEIAlign  -->
	<xsl:variable name="right">
	<xsl:value-of select="if($pathToTEIAlign eq '') then 
	(concat('./', $paralleltext, '/', $fromLang, '-',  $toLang, '.links'))
	else
	(
	concat($pathToTEIAlign, '/', $paralleltext, '_', upper-case(substring($fromLang,1,2)), substring($fromLang,3,1), '-', upper-case(substring($toLang,1,2)), substring($toLang,3,1) ,'.alg.xml' )
	)	"/>
	</xsl:variable>
	<xsl:variable name="wrong">
	<xsl:value-of select="if($pathToTEIAlign eq '') then 
	(concat('./', $paralleltext, '/', $toLang, '-',  $fromLang, '.links'))
	else
	(concat($pathToTEIAlign, '/', $paralleltext, '_', upper-case(substring($toLang,1,2)), substring($toLang,3,1), '-', upper-case(substring($fromLang,1,2)), substring($fromLang,3,1) ,'.alg.xml' ))
	"/>
	</xsl:variable>
	<xsl:message><xsl:value-of select="concat('Reading link file ', (if (doc-available($right)) then ($right) else ($wrong)) )"/></xsl:message>


	<xsl:for-each select="doc(if (doc-available($right)) then ($right) else ($wrong))//link">
	<xsl:element name="link">
	<xsl:attribute name="xtargets" select="@xtargets"/>
	<xsl:if test="matches(@xtargets, '^;')">
	<xsl:attribute name="special" select="if (doc-available($right)) then ('noSource') else ('noTarget')"/>
	</xsl:if>
	<xsl:if test="matches(@xtargets, ';$')">
	<xsl:attribute name="special" select="if (doc-available($right)) then ('noTarget') else ('noSource')"/>
	</xsl:if>
	
	<xsl:variable name="curLink">
	<xsl:value-of select="@xtargets"/>
	</xsl:variable>
	<!-- <xsl:attribute name="orig" select="@xtargets"/> -->
	<xsl:analyze-string select="replace($curLink, '(.*?);(.*)', if (doc-available($right)) then ('$1') else ('$2'))" regex="([^ ]+)">
	<xsl:matching-substring>
	<xsl:element name="fromS">
	<xsl:value-of select="regex-group(1)"/>
	</xsl:element>
	<xsl:analyze-string select="replace($curLink, '(.*?);(.*)', if (doc-available($right)) then ('$2') else ('$1'))" regex="([^ ]+)">
	<xsl:matching-substring>
	<xsl:element name="toS">
	<xsl:value-of select="regex-group(1)"/>
	</xsl:element>
	</xsl:matching-substring>
	</xsl:analyze-string>
	</xsl:matching-substring>
	</xsl:analyze-string>
	
	
	

	<xsl:if test="not ($onlySent eq 'yes')">
	<xsl:for-each select="wordLink">

	<!-- jetzt alignmentstring auseinandernehmen -->
	<xsl:variable name="curLink">
	<xsl:value-of select="@xtargets"/>
	</xsl:variable>
	<xsl:element name="wordlink">
	<!-- <xsl:attribute name="orig" select="@xtargets"/> -->
	<xsl:analyze-string select="replace($curLink, '(.*?);(.*)', if (doc-available($right)) then ('$1') else ('$2'))" regex="([^+]+)">
	<xsl:matching-substring>
	<xsl:element name="fromWord">
	<xsl:value-of select="regex-group(1)"/>
	<xsl:analyze-string select="replace($curLink, '(.*?);(.*)', if (doc-available($right)) then ('$2') else ('$1'))" regex="([^+]+)">
	<xsl:matching-substring>
	<xsl:element name="toWord">
	<xsl:value-of select="regex-group(1)"/>
	</xsl:element>
	</xsl:matching-substring>
	</xsl:analyze-string>
	</xsl:element>
	</xsl:matching-substring>
	</xsl:analyze-string>
	</xsl:element>
	</xsl:for-each>
	</xsl:if>
	</xsl:element>
	</xsl:for-each>
	</xsl:element>

	</xsl:template>
	
	<xsl:template name="checkForZerolinks">
	<xsl:param name="lastLink"/>
	<xsl:if test="$lastLink/preceding-sibling::link[1][@special eq 'noSource']">
	<xsl:element name="BegAlign">
	<xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:attribute name="id" select="'ZERO1'"/>
	</xsl:element>
	<xsl:element name="w"><xsl:value-of select="'--'"/></xsl:element>
	<xsl:element name="EndAlign">
	<xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:attribute name="id" select="'ZERO1'"/>
	</xsl:element>
	<xsl:call-template name="checkForZerolinks">
	<xsl:with-param name="lastLink" select="$lastLink/preceding-sibling::link[1][@special eq 'noSource']"/>
	</xsl:call-template>
	</xsl:if>
	</xsl:template>
	
	
	<xsl:template match="/">

<!-- read in all forms using the template -->
 	<xsl:variable  name="forms"> 
	<xsl:for-each select="$parameter/lng">
 	<xsl:call-template name="getForms">
	<xsl:with-param name="lang" select="."/>
	<xsl:with-param name="posRegexp" select="'.'"/>
	</xsl:call-template>
	</xsl:for-each>
	</xsl:variable>
<!--
 <xsl:result-document href="{string(concat('./', $paralleltext, '/','forms.xml'))}" method="xml" encoding="utf8">
<xsl:copy-of select="$forms"/>
</xsl:result-document> 
-->

 	<xsl:variable  name="links"> 
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:call-template name="getLinks">
	<xsl:with-param name="fromLang" select="$parameter/lng[@primary]"/>
	<xsl:with-param name="toLang" select="."/>
	</xsl:call-template>
	</xsl:for-each>
	</xsl:variable>

	<!-- 
<xsl:result-document href="{concat ('./', $paralleltext, '/', 'links.xml')}" method="xml" encoding="utf8">
<xsl:copy-of select="$links"/>
</xsl:result-document> 
-->
<xsl:message><xsl:value-of select="'Now writing out'"/></xsl:message>
<xsl:variable name="suffix">
<xsl:if test="$explicitename eq 'yes'">
<xsl:value-of select="$parameter/lng[@primary]"/>
<xsl:value-of select="'-'"/>
</xsl:if>
</xsl:variable>

	<!-- 
	<xsl:result-document href="{concat ('./', $paralleltext,'-', $description,'-',$suffix, current-dateTime(), '.xml')}" method="xml" encoding="utf8">
-->
	<xsl:result-document href="{concat ($outputdir,'/', $paralleltext,'-', $description,'-',$suffix, if($timestamp eq 'yes') then ( current-dateTime()) else (), '.xml')}" method="xml" encoding="utf8">

<xsl:element name="corpus">
<xsl:attribute name="text" select="$paralleltext"/>
<xsl:attribute name="lngs">
<xsl:value-of select="$parameter/lng[@primary]"/>
<xsl:value-of select="'::'"/>
<xsl:for-each select="$parameter/lng[not (@primary)]">
<xsl:value-of select="."/>
<xsl:value-of select="if (position() eq last()) then () else (':')"/>
</xsl:for-each>
</xsl:attribute>

<!-- 
	<xsl:for-each select="$parameter/lng[@primary]">
	<xsl:value-of select="'ASP'"/>
	<xsl:value-of select="upper-case(.)"/>
	</xsl:for-each>

	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:value-of select="'&#9;'"/>
	<xsl:value-of select="'ASP'"/>
	<xsl:value-of select="upper-case(.)"/>
	</xsl:for-each>
<xsl:text>
</xsl:text>
 -->
 
	
<!-- now write out all pairs present in the input file-->
	<xsl:for-each select="$forms/lang[@lng = $parameter/lng[@primary]]/s">
	<xsl:variable name="curS_id" select="@id"/>
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:variable name="actLng" select="."/>
	<!--<xsl:if test="$links/links[@toLang eq $actLng]/link[fromS = curS_id]/@xalign[matches (., '$beginAlignRegex')]">-->


	<!-- This needs to be replaced by keys...
	<xsl:if test="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]">
	-->
	
	<!-- Are we at first element of  alignment ? -->
	
	<xsl:if test="key('getLinks', $curS_id, key('getLinksbyLNG',  $actLng,$links))[fromS[1] eq $curS_id]">
	<!--
	<xsl:if test="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]">
	replaced by key! -->

	<!-- Recursively check whether we need to provide for preceding 0:1 links -->
	<xsl:call-template name="checkForZerolinks">
	<xsl:with-param name="lastLink" select="key('getLinks', $curS_id, key('getLinksbyLNG',  $actLng,$links))[fromS[1] eq $curS_id]"/>
	<!--
	<xsl:with-param name="lastLink" select="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]"/>
	replaced by key! -->
	</xsl:call-template>

		
	
	<!-- give alignment input link-->
	<xsl:element name="BegAlign">
	<xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:if test="$testing eq 'yes'">
	<xsl:attribute name="id" select="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]/@xtargets"/>
	</xsl:if>
	</xsl:element>
	</xsl:if>
	</xsl:for-each> 
	
	<xsl:element name="s">
	<xsl:attribute name="id" select="@id"/>
	<xsl:if test="(@id mod 100) eq 0">
	<xsl:message><xsl:value-of select="@id"/></xsl:message>
	</xsl:if>

	<xsl:for-each select="w">
	<xsl:element name="w">
	<xsl:attribute name="id" select="@id"/>
	<xsl:attribute name="lem" select="@lem"/>
	<xsl:attribute name="pos" select="@pos"/>
	<xsl:value-of select="text()"/>
	<!-- Only if we don't happen to be dealing with only the sent-aligned stuff -->
	<xsl:if test="not ($onlySent eq 'yes')">
	<xsl:element name="waligns">
	<xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
	<xsl:variable name="s_id"><xsl:value-of select="replace(@id, '(\d+)\.(\d+)','$1')"/></xsl:variable>
	<!-- aligned langs -->
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:element name="walign">
	<xsl:attribute name="lng" select="."/>
	<xsl:variable name="actLng" select="."/>
	<!-- <xsl:message><xsl:value-of select="$s_id"/></xsl:message>-->
	<xsl:variable name="toS" select="key('getLinks', $s_id, key('getLinksbyLNG',  $actLng,$links))/toS"/>
	<!--
	<xsl:variable name="toS" select="$links/links[@toLang eq $actLng]/link[fromS = $s_id]/toS"/>
	<xsl:with-param name="lastLink" select="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[1] eq $curS_id]"/>
	replaced by key! -->

	
	<!-- <xsl:message><xsl:value-of select="$toS"/></xsl:message>-->
	<xsl:for-each  select="key('getTargetForms', $toS, $forms/lang[@lng = $actLng])/w[@id[. = key('getLinks', $s_id, key('getLinksbyLNG',  $actLng,$links))/wordlink/fromWord[text() =$id]/toWord]]">
	<!--
	replaced by key
	<xsl:for-each  select="$forms/lang[@lng = $actLng]/s[@id = $toS]/w[@id[. = key('getLinks', $s_id, $links/links[@toLang eq $actLng])/wordlink/fromWord[text() =$id]/toWord]]">
		key('getTargetForms', $toS, $forms/lang[@lng = $actLng])
	-->
	<!--
	<xsl:for-each  select="$forms/lang[@lng = $actLng]/s[@id = $toS]/w[@id[. = 
	$links/links[@toLang eq $actLng]/link[fromS = $s_id]
	/wordlink/fromWord[text() =$id]/toWord]]">
	replaced by key! -->

	<xsl:copy-of select="."/>
	</xsl:for-each> 
	</xsl:element>
	</xsl:for-each>
	</xsl:element>
	</xsl:if>
	</xsl:element>
	</xsl:for-each>
	</xsl:element>
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:variable name="actLng" select="."/>
	<!--<xsl:if test="$links/links[@toLang eq $actLng]/link[fromS = curS_id]/@xalign[matches (., '$beginAlignRegex')]">-->
	<xsl:if test="	key('getLinks', $curS_id, key('getLinksbyLNG',  $actLng,$links))	[fromS[last()] eq $curS_id]">
	<!--
	key('getLinks', $curS_id, $links/links[@toLang eq $actLng])
	<xsl:if test="
	$links/links[@toLang eq $actLng]/link[fromS = $curS_id]
	key('getLinks', $curS_id, $links/links[@toLang eq $actLng])
	[fromS[last()] eq $curS_id]">
	replaced by key! -->

	<xsl:element name="EndAlign">
	<xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:if test="$testing eq 'yes'">
	<xsl:attribute name="id" select="$links/links[@toLang eq $actLng]/link[fromS = $curS_id][fromS[last()] eq $curS_id]/@xtargets"/>
	</xsl:if>
	</xsl:element>
	</xsl:if>
	</xsl:for-each> 

	<!--  are we at last s? if so, check for  trailing zero link -->
	<xsl:if test="position() eq last()">
	<!--  go through all aligned lngs and put beg align tag-->
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:variable name="actLng" select="."/>
	<xsl:if test="key('getLinksbyLNG',  $actLng,$links)/link[position() eq last()][@special eq 'noSource']">
	<!--  if the last one is a zero link, the preceding ones might also be... -->
	<xsl:call-template name="checkForZerolinks">
	<xsl:with-param name="lastLink" select="key('getLinksbyLNG',  $actLng,$links)/link[position() eq last()]"/>
	</xsl:call-template>

	
	<xsl:element name="BegAlign"><xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:if test="$testing eq 'yes'"><xsl:attribute name="id" select="'ZEROLINKLAST'"/></xsl:if>
	</xsl:element>
	</xsl:if>
	</xsl:for-each>
	<xsl:element name="w"><xsl:value-of select="'--'"/></xsl:element>
	<!--  go through all aligned lngs and put end align tag-->
	<xsl:for-each select="$parameter/lng[not (@primary)]">
	<xsl:variable name="actLng" select="."/>
	<xsl:if test="key('getLinksbyLNG',  $actLng,$links)/link[position() eq last()][@special eq 'noSource']">
	<xsl:element name="EndAlign"><xsl:attribute name="lng" select="concat(upper-case(substring($parameter/lng[@primary],1,2)), substring($parameter/lng[@primary],3,1), '_',upper-case(substring(.,1,2)), substring(.,3,1) )"/>
	<xsl:if test="$testing eq 'yes'"><xsl:attribute name="id" select="'ZEROLINKLAST'"/></xsl:if>
	</xsl:element>
	</xsl:if>
	</xsl:for-each>
	</xsl:if>

	
	</xsl:for-each>
</xsl:element>
	</xsl:result-document>

	
		
	</xsl:template>

	
	
	
</xsl:stylesheet>
