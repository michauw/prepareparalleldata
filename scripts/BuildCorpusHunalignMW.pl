use File::Spec::Functions;
use File::Copy;
use Cwd;
use open ':utf8';

#zunдchst Argument parsen
#alle Argumente in einen Hash, dann sind sie leicht abzufragen
for $par (@ARGV){
	#parameterьbergabe?
	if ($par  =~ /^(.+)=(.+)$/){
		$args{$1}=$2;
	}
	else {#Schalter bzw. Dateiname
		$args{$par}=1;
	}
}

print "Builds a corpus using HUNALIGN";
print "\nschreibt die entsprechenden InputDateien fьr die Alignierung per BSA";
print "\n in eine Verzeichnisstruktur unter BSA.";
print "Argumente: Directory [-silent] [-nolemma] [-wdir=] [-odir=]";



#here are the files to be aligned and processed
$dir = $ARGV[0];
$rpcdir=$dir;

#the working directory
if (defined $args{'-wdir'}){
	$aligDir=$args{'-wdir'};
} 
else {
	$aligDir="HUNALIGNING"; 
}


if (defined $args{'-odir'}){
	$odir=$args{'-odir'};
} 
else {
         $odir="Texte-ALG-TST";
}



#$rpcdir="Texte-rpc";


$batchdatei="allbatch";

if (defined $args{'-hunpath'}){
	$alignmenttool=$args{'-hunpath'};
}
else {
	$alignmenttool="/home/parallel/corpbuild/hunalign-1.2/src/hunalign/hunalign";
}
#$alignmenttool="hunalign.exe";

#now convert all those files....
print "\n----calling prepare script ---\n";
$cont = "";
$cont = "-c" if (defined $args{'-c'});
if (defined $args{'-nolemma'}){
	system ("perl Prepare-RPC-for-HunalignSERVER.pl -idir=$dir -odir=$aligDir -batch $cont");
} else{
	system ("perl Prepare-RPC-for-HunalignSERVER.pl -idir=$dir -odir=$aligDir -lemma -batch $cont");
}	

(opendir DIR, $dir) || die("Could not open directory $dir\n"); 
		@all_rpc_files = grep /.rpc$/, readdir DIR;
closedir(DIR);

foreach $file_name (@all_rpc_files) {
		#print $file_name;
		$filenames{$file_name}++;
		$file_name =~ /(.*)_(...?)\.rpc$/;
		$file_name_body{$1}++;
		$language_tag{"$2"}++;
}

#print %language_tag;
@languages= (sort (keys %language_tag));
@file_name_bodies=(sort(keys %file_name_body));
print "\n".($#languages+1)." Sprachen, ".($#languages_alt +1)." Varianten gefunden, suche Kombinationen\n";

$batchfile=catfile($aligDir, $batchdatei);
open (BATCH, ">$batchfile") or die ("couldn't open batch file");

#make an empty dictionary....
$dicfile=catfile($aligDir, "null.dic");
open (DUM, ">$dicfile"); close DUM;

for $lngNr (0..@languages-1){
	for $lngNr2 ($lngNr+1..@languages-1){
		print "$languages[$lngNr]\t $languages[$lngNr2]";
		for  $fileBody(@file_name_bodies){
			$lngName1= $languages[$lngNr];
			$lngName2= $languages[$lngNr2];
			
			unless (defined ($args{'-c'}) and ((-f catfile ($aligDir, "$fileBody\_$lngName1-$lngName2.hun")) or (-f catfile ($aligDir, "$fileBody\_$lngName2-$lngName1.hun"))))
			{
				$currFile1="$fileBody\_$lngName1.rpc";
				$currFile2="$fileBody\_$lngName2.rpc";

				#both versions there?
				if ((defined ($filenames{$currFile1})) &&(defined ($filenames{$currFile2}))){
					#write out the batch file line...
					$currFile1=catfile ($aligDir,"$fileBody\_$lngName1.snt");
					$currFile2=catfile ($aligDir,"$fileBody\_$lngName2.snt");
					$algFile=catfile ($aligDir, "$fileBody\_$lngName1-$lngName2.hun");
					$batchline="$currFile1\t$currFile2\t$algFile\n";
					print BATCH $batchline ;
					print "\n";
				}
			}
			else {print "   Skipping...\n";}
		}
	}
}

#$batchfile=catfile($aligDir, "allbatch1");

if ($ARGV[1] eq '-full'){
	print "\nOK, starting Alignment....\n";
	
	print ("$alignmenttool -utf -ppthresh=10 -topothresh=10 -realign -batch $dicfile $batchfile ");
	system ("$alignmenttool -utf -ppthresh=10 -topothresh=10 -realign -batch $dicfile $batchfile ");

#job	system ("hunalign.exe -ppthresh=30 -headerthresh=100 -topothresh=30 -realign -batch HUNALIGNING\null.dic HUNALIGNING\allbatch");
	print"\nOK, extracting ALG-files\n";
	system ("perl Hunalign2RPC.pl -batch -multidir -idir=$aligDir -odir=$odir -rpcDir=$rpcdir");
	print"\nOK, extracting CWB-files\n";
#	system ("perl MultiDirRpc2CWB.pl Texte-ALG Texte-CWB");
	print"\nOK, extracting PAR-files\n";
#	system ("perl MultiDirRpc2ParaConc.pl Texte-ALG Texte-PAR");
	
}

#OK, jetzt wird fьr die Varianten ein virtuelles Korpus gebaut, dann kann aligniert werden



#zunдchst die Varianten untereinander
#for $lngNr (0..@languages_alt-1){
#	for $lngNr2 ($lngNr+1..@languages_alt-1){
#		print "$languages_alt[$lngNr]\t $languages_alt[$lngNr2]\n";
#		for  $file_body(@file_name_bodies){
#			#jetzige (alternative) Endungen
#			$altLng1= $languages_alt[$lngNr];
#			$altLng2= $languages_alt[$lngNr2];
#			
#			}
#		}
#	}
#}


#zunдchst die Varianten untereinander
#for $lngNr (0..@languages_alt-1){
#	for $lngNr2 (0..@languages_all-1){
#		if (not $languages_alt[$lngNr] eq $languages_all[$lngNr2])
#		print "$languages_alt[$lngNr]\t $languages_all[$lngNr2]\n";
#		for  (@file_name_bodies){
#			$currFile1="$_\_$languages_alt[$lngNr].rpc";
#			$currFile2="$_\_$languages_all[$lngNr2].rpc";
#			if ((defined ($filenames{$currFile1})) &&(defined ($filenames{$currFile2}))){
#				copy ((catfile ($dir, $currFile1)), (catfile ($tmpDir, "$_\=$languages_alt[$lngNr]\=$languages_all[$lngNr2]\=\_$languages_alt[$lngNr].rpc")));
#				copy ((catfile ($dir, $currFile2)), (catfile ($tmpDir, "$_\=$languages_alt[$lngNr]\=$languages_all[$lngNr2]\=\_$languages_all[$lngNr2].rpc")));
#			}
#		}	 		
#	}
#}

	