import os, math, time, sys, re
import codecs
import argparse
import xml.etree.ElementTree as ET
#import asizeof as size

VER = 1.3

INDENT = '  '

def quote (text, onlyQuote = False):
    if onlyQuote and '"' in text:
        return re.sub ('"', r'&#34;', text)
    restricted = {"'": r'&#39;', '"': r'&#34;', '&': r'&#38;', '<': r'&#60;', '>': r'&#62;'}
    for i in range (len (text)):
        if text[i] in restricted:
            text = text[:i] + restricted[text[i]] + text[i+1:]
    return text

def indent (elem, level = 0):
    i = "\n" + level * INDENT
    if len (elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def writeToFile (top, outdir, filename):
    
    if not os.path.exists (outdir):
        os.makedirs (outdir)
        
    with open (os.path.join (outdir, filename), 'w') as f:
        f.write ('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.write ('<' + ' '.join ([top.tag] + ['="'.join (el) + '"' for el in top.items ()]) + '>\n' + INDENT)
        for elem in list (top):
            indent (elem, 1)
            f.write (''.join (ET.tostringlist (elem, 'utf8', 'xml')[1:]))
        f.seek (-len (INDENT), 2)
        f.write ('</' + top.tag + '>')
        

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    indent (elem)
    return ET.tostring (elem, 'utf-8')

##def getLinks (path, isStraight):
##    data = ET.parse (path)
##    links = {}
##    zeros = []
##    for link in data.iter ('wordLink'):
##            if isStraight:
##                main, secondary = [el.split ('+') for el in link.get ('xtargets').split (';')]
##            else:
##                secondary, main = [el.split ('+') for el in link.get ('xtargets').split (';')]
##            for word in main:
##                links[word] = secondary
##    linkSent = []
##    for slink in data.iter ('link'):
##        if isStraight:
##            main, secondary = [el.split () for el in slink.get ('xtargets').split (';')]
##        else:
##            secondary, main = [el.split () for el in slink.get ('xtargets').split (';')]
##        if len (main) == 0:
##            linkSent.append ('0')
##        elif len (main) == 1:
##            linkSent.append ('BE')
##        else:
##            linkSent.append ('B')
##            for sent in main[1:-1]:
##                linkSent.append ('-')
##            linkSent.append ('E')
##            
##    return links, linkSent

def getLinks (path, isStraight):
    links = {}
    linkSent = []
    with open (path) as fin:
        patsent = re.compile (r'<link.*xtargets\s*=\s*"(.*?)"')
        patwords = re.compile (r'<wordLink.*xtargets\s*=\s*"(.*?)"')
        for line in fin:
            r = re.search (patwords, line)
            if r:
                targets = r.group (1)
                if isStraight:
                    main, secondary = [el.split ('+') for el in targets.split (';')]
                else:
                    secondary, main = [el.split ('+') for el in targets.split (';')]
                for word in main:
                    links[word] = secondary
                continue
            r = re.search (patsent, line)
            if r:
                targets = r.group (1)
                if isStraight:      
                    main, secondary = [el.split () for el in targets.split (';')]
                else:
                    secondary, main = [el.split () for el in targets.split (';')]
                if len (main) == 0:
                    linkSent.append ('0')
                elif len (main) == 1:
                    linkSent.append ('BE')
                else:
                    linkSent.append ('B')
                    for sent in main[1:-1]:
                        linkSent.append ('-')
                    linkSent.append ('E')
            
    return links, linkSent

def getSecondary (data):
    words = {}
    for word in data.iter ('w'):
        words[word.get ('id')] = [quote (word.get ('lem'), True), quote (word.get ('pos'), True), quote (word.text)]
    return words

def getSentWords (xmlStruct):
    res = []
    for sentence in xmlStruct.findall ('./p/s'):
        words = []
        for word in sentence.findall ('w'):
            words.append ([quote (word.get ('lem'), True), quote (word.get ('pos'), True), quote (word.text)])
        res.append (words)
    return res

def prepareData (langs, textName, teiPath):
    dataMain = {}
    dataLang = {}
    for lang in langs:
        xmlPath = os.path.join (teiPath, textName + '_' + lang.upper () + '.rpc.xml')
        if not os.path.exists (xmlPath):
            xmlPath = os.path.join (teiPath, textName + '_' + lang + '.rpc.xml')
        if not os.path.exists (xmlPath):
            xmlPath = os.path.join (teiPath, textName + '_' + lang[:-1].upper () + lang[-1] + '.rpc.xml')
        print lang + '\n----'
        print 'parsing as main...   ',
        mainXml = ET.parse (xmlPath)
        dataMain[lang] = getSentWords (mainXml)
        print 'ok'
        print 'parsing as aligned...   ',
        dataLang[lang] = getSecondary (mainXml)
        print 'ok'
    print '\ndone.\n'
    return dataMain, dataLang

def makeAlignment (langs, textName, linkPath, outdir, dataMain, dataLang, onlySent = False, limit = 0):
    mainLang = langs[0]
    if not os.path.exists (outdir):
        os.makedirs (outdir)
    print os.path.abspath (outdir)
    print 'Alignment:', mainLang
    fout = codecs.open (os.path.join (outdir, '%s-FullAlignmentXML-%s.xml' % (textName, mainLang)), 'w', 'utf8')
    fout.write ('<?xml version="1.0" encoding="UTF-8"?>\n')
    fout.write ('<corpus text="%s" lngs="%s">\n' % (textName, mainLang + '::' + ':'.join (langs[1:])))
    langShifts = {}
    for lang in langs[1:]:
        langShifts[lang] = 0
    dataLink = {}
    sentNodes = []
    for lang in langs[1:]:
        isStraight = True
        linkRpc = os.path.join (linkPath, mainLang + '-' + lang + '.links')
        if not os.path.exists (linkRpc):
            linkRpc = os.path.join (linkPath, lang + '-' + mainLang + '.links')
            isStraight = False
        if not onlySent:
            print 'Parsing links'
            dataLink[lang] = getLinks (linkRpc, isStraight)
    done = 0
    sentences = dataMain[mainLang]
    #print 'dataLink:', size.asizeof (dataLink)
    if limit:
            sentences = sentences[:limit]
    print '%d sentences' % len (sentences)
    for isent, sent in enumerate (sentences):
            tmp = math.floor (float (isent) / len (sentences) * 100)
            if tmp > done:
                done = tmp
                print done, '%'
            for lang in langs[1:]:
                if isent + langShifts[lang] >= len (dataLink[lang][1]):
                    continue
                while dataLink[lang][1][isent + langShifts[lang]] == '0':
                    #ET.SubElement (root, 'BegAlign', {'lng': mainLang.upper() + '_' + lang.upper(), 'id': 'ZERO1'})
                    fout.write (INDENT + '<BegAlign lng="%s" id="ZERO1"/>\n' % (mainLang.upper() + '_' + lang.upper()))
                    fout.write (INDENT + '<w>--</w>\n')
                    fout.write (INDENT + '<EndAlign lng="%s" id="ZERO1"/>\n' % (mainLang.upper() + '_' + lang.upper()))
                    #wzero.text = '--'
                    #ET.SubElement (root, 'EndAlign', {'lng': mainLang.upper() + '_' + lang.upper(), 'id': 'ZERO1'})
                    langShifts[lang] += 1
                if dataLink[lang][1][isent + langShifts[lang]] in ['B', 'BE']:
                    #ET.SubElement (root, 'BegAlign', {'lng': mainLang.upper() + '_' + lang.upper()})
                    fout.write (INDENT + '<BegAlign lng="%s"/>\n' % (mainLang.upper() + '_' + lang.upper()))
            fout.write (INDENT + '<s id="%d">\n' % isent)
            for iword, word in enumerate (sent):
                wId = str (isent) + '.' + str (iword + 1)
                fout.write (INDENT * 2 + '<w id="%s" lem="%s" pos="%s">%s' % (tuple ([wId] + word)))
                #curWord.text = word[2]
                if not onlySent:
                    fout.write ('<waligns>\n')
                    for lang in langs[1:]:
                        fout.write (INDENT * 4 + '<walign lng="%s"' % lang)
                        try:
                            if wId in dataLink[lang][0]:
                                fout.write ('>\n')
                            for alignNumber in dataLink[lang][0][wId]:
                                align = dataLang[lang][alignNumber]
                                fout.write (INDENT * 5 + '<w id="%s" lem="%s" pos="%s">%s</w>\n' % (alignNumber, align[0], align[1], align[2]))
                        except KeyError:
                                fout.write ('/>\n')
                                continue
                        fout.write (INDENT * 4 + '</walign>\n')
                    fout.write (INDENT * 3 + '</waligns>\n')
                else:
                    fout.write ('\n')
                fout.write (INDENT * 2 + '</w>\n')
            fout.write (INDENT + '</s>\n')
            for lang in langs[1:]:
                if isent + langShifts[lang] >= len (dataLink[lang][1]):
                    continue
                if dataLink[lang][1][isent + langShifts[lang]] in ['E', 'BE']:
                    fout.write (INDENT + '<EndAlign lng="%s"/>\n' % (mainLang.upper() + '_' + lang.upper()))
    langsLeft = langs[1:]
    isent += 1
    while langsLeft:
        for lang in langs[1:]:
            if not lang in langsLeft:
                continue
            try:
                symbol = dataLink[lang][1][isent + langShifts[lang]]
                if symbol in ['0', 'B', 'BE']:
                    fout.write (INDENT + '<BegAlign lng="%s"/>\n' % (mainLang.upper() + '_' + lang.upper()))
                if symbol == '0':
                    fout.write (INDENT + '<w>--</w>\n')
                if symbol in ['0', 'E', 'BE']:
                    fout.write (INDENT + '<EndAlign lng="%s"/>\n' % (mainLang.upper() + '_' + lang.upper()))
                langShifts[lang] += 1
            except IndexError:
                del langsLeft[langsLeft.index (lang)]
    fout.write ('</corpus>')
    print '100 %'
    #print 'writing...'
    #with open ('%s-FullAlignmentXML-%s.xml' % (textName, mainLang), 'w') as f:
            #f.write ('<?xml version="1.0" encoding="UTF-8"?>\n')
        #f.write (prettify (root))
    #writeToFile (root, outdir, '%s-FullAlignmentXML-%s.xml' % (textName, mainLang))
    fout.close ()
    print 'done.'

if __name__ == '__main__':

    options = {'pathToTEICorpus': r'D:\Praca\parandelfels\tmp\LM\TexteRpc', 'paralleltext': 'LesMiserables', 'lngs': '', 'pathToTEIAlign': r'D:\Praca\parandelfels\tmp\LM\LesMiserables',
               'onlySent': '', 'timestamp': '', 'outputdir': r'CORPUS-OUTP', 'verbose': '', 'limit': 0,
               'explicitname': '', 'testing': '', 'lngs': 'du en es fr hu it pl pt ro ru', 'allCombinations': 'yes'}
    required = []#'pathToTEICorpus', 'pathToTEIAlign']
    for arg in sys.argv[1:]:
        try:
            name, value = arg.split ('=')
            options[name] = value
        except:
            print 'Malformed parameter:', arg
        if not name in options:
            print 'Wrong parameter:', name
            sys.exit (1)
        if name in required:
            del required[required.index (name)]
    for missing in required:
        print 'Missing parameter:', missing
    if required:
        sys.exit (1)
    
    text = options['paralleltext']
    teiPath = options['pathToTEICorpus']
    linkPath = options['pathToTEIAlign']
    langs = options['lngs'].split ()
    outputdir = options['outputdir']
    limit = int (options['limit'])
    sentences = options['onlySent'] == 'yes'    
    if len (langs) < 2:
        print 'Error: you have to specify at least 2 languages!'
        sys.exit (1)
    start = time.time ()
    dataMain, dataLang = prepareData (langs, text, teiPath)
    # print 'dataMain:', size.asizeof (dataMain)
    # print 'dataLang:', size.asizeof (dataLang)

    if options['allCombinations'] != 'yes':
        makeAlignment (langs, text, linkPath, outputdir, dataMain, dataLang, sentences, limit)
    else:
        allCombs =[]
        print 'All combinations...'
        for i in range (len (langs)):
            comb = [langs[i]]
            for j in range (len (langs)):
                if (j != i):
                    comb.append (langs[j])
            allCombs.append (comb)
        for comb in allCombs:
            print comb
            makeAlignment (comb, text, linkPath, outputdir, dataMain, dataLang, sentences, limit)
            
    print 'time: ', time.time () - start
