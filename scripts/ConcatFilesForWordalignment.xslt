<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
	<!-- <xsl:strip-space elements="*"/> -->
	<xsl:preserve-space elements="*"/>
	<xsl:param name="lngs" select="'ru uk by pl cz sk sl hr sr mk bg'"/>
	<xsl:param name="TEICorpusPath" select="'../Texte-rpc-TEI-SHORTTAG'"/>
	<xsl:param name="TEIAlignPath" select="'../Texte-ALG-TEI'"/>
	<xsl:param name="OUTPUTPATH" select="'./HALLO'"/>
	<xsl:param name="FILELIST" select="'FILELIST.xml'"/>
	
	<xsl:variable name="parameter">
	<!-- <lng subsetReg="^Vmm" notLemReg="^быти|пожаловать|прощать|извинить|тереть|позволить|помиловать|изволить|просить$" primary="yes">ru</lng> -->
		<xsl:analyze-string select="$lngs" regex="(\w+)">
			<xsl:matching-substring>
				<xsl:element name="lng">
					<xsl:if test="position() eq 1">
						<xsl:attribute name="primary" select="'yes'"/>
					</xsl:if>
					<xsl:value-of select="regex-group(1)"/>
				</xsl:element>
			</xsl:matching-substring>
		</xsl:analyze-string>
	</xsl:variable>
	


	<xsl:variable name="corpFiles">
		<xsl:copy-of select="doc($FILELIST)"/>
	</xsl:variable>


	<xsl:template match="/">
		<xsl:variable name="sortedLNGS">
				<xsl:for-each select="$parameter/lng">
			<xsl:sort/>
				<xsl:copy-of select="."/>
		</xsl:for-each>
    </xsl:variable>
			<xsl:message >
			<xsl:copy-of select="$sortedLNGS"/>
			</xsl:message>

			<xsl:for-each select="(1 to count($sortedLNGS/lng)-1)">
			<xsl:variable name="first" select="."/>
			<xsl:for-each select="((.+1) to count($sortedLNGS/lng)-1)">
			<!--<xsl:message >
			<xsl:copy-of select="., $first"/>
			</xsl:message>-->
			

			<xsl:variable name="lngpair">
					<xsl:element name="lng">
						<xsl:value-of select="$sortedLNGS/lng[position()=$first]"/>
					</xsl:element>
					<xsl:element name="lng">
						<xsl:value-of select="$sortedLNGS/lng[position()=current()]"/>
					</xsl:element>

					</xsl:variable>


			<xsl:variable name="lng1" select="replace($lngpair/lng[1], '\^', '')"/>
			<xsl:variable name="lng2" select="replace($lngpair/lng[2], '\^', '')"/>

			<xsl:variable name="lng1" select="$lngpair/lng[1]"/>
			<xsl:variable name="lng2" select="$lngpair/lng[2]"/>

			<xsl:message>
				<xsl:copy-of select="$lng1"/>
				<xsl:copy-of select="$lng2"/>
			</xsl:message>


			<xsl:if test="$corpFiles/COMBOS/COMBI[matches(@lng1, $lngpair/lng[1])][matches(@lng2, $lngpair/lng[2])]/COMPL">

<!-- Now we enter this only if there is such a combination. We write to three files..  -->

<!-- First lng file.. -->

				<xsl:result-document href="{concat($OUTPUTPATH, '/',$lng1, '-', $lng2, '/', $lng1, '/ALL.xml')}">
					<xsl:element name="text">
						<xsl:for-each select="$corpFiles/COMBOS/COMBI[matches(@lng1, $lngpair/lng[1])][matches(@lng2, $lngpair/lng[2])]/COMPL">
							<xsl:variable name="textname" select="concat(replace(@file1, '^(.)(.*)?_.*', '$1'), position())"/>
							<xsl:for-each select="doc(concat($TEICorpusPath, '/', @file1))/text//s">
								<xsl:element name="s">
									<xsl:attribute name="id" select="concat($textname, ':', @id)"/>
									<xsl:for-each select="w">
										<xsl:element name="w">
											<xsl:attribute name="id" select="concat($textname, ':', @id)"/>
											<xsl:copy-of select="@*[name()!='id']"/>
											<xsl:copy-of select="text()"/>
										</xsl:element>
									</xsl:for-each>
								</xsl:element>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:element>
				</xsl:result-document>

<!-- Second lng file.. -->

				<xsl:result-document href="{concat($OUTPUTPATH, '/', $lng1, '-', $lng2, '/', $lng2, '/ALL.xml')}">
					<xsl:element name="text">
						<xsl:for-each select="$corpFiles/COMBOS/COMBI[matches(@lng1, $lngpair/lng[1])][matches(@lng2, $lngpair/lng[2])]/COMPL">
							<xsl:variable name="textname" select="concat(replace(@file1, '^(.)(.*)?_.*', '$1'), position())"/>
							<xsl:for-each select="doc(concat($TEICorpusPath, '/', @file2))/text//s">
								<xsl:element name="s">
									<xsl:attribute name="id" select="concat($textname, ':', @id)"/>
									<xsl:for-each select="w">
										<xsl:element name="w">
											<xsl:attribute name="id" select="concat($textname, ':', @id)"/>
											<xsl:copy-of select="@*[name()!='id']"/>
											<xsl:copy-of select="text()"/>
										</xsl:element>
									</xsl:for-each>
								</xsl:element>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:element>
				</xsl:result-document>

<!-- alg file..  -->
				<xsl:result-document href="{concat($OUTPUTPATH,'/', $lng1, '-', $lng2, '/',  $lng1, '-', $lng2, '.xml')}">
					<xsl:element name="cesAlign">
						<xsl:element name="linkGrp">
							<xsl:attribute name="targType" select="'s'"/>
							<xsl:attribute name="toDoc" select="concat($lng2, '/ALL.xml')"/>
							<xsl:attribute name="fromDoc" select="concat($lng1, '/ALL.xml')"/>
							<xsl:for-each select="$corpFiles/COMBOS/COMBI[matches(@lng1, $lngpair/lng[1])][matches(@lng2, $lngpair/lng[2])]/COMPL">
								<xsl:variable name="textname" select="concat(replace(@file1, '^(.)(.*)?_.*', '$1'), position())"/>
								<xsl:variable name="repltextname" select="concat('$1',replace(@file1, '^(.)(.*)?_.*', '$1'), position(),':$2')"/>
								<xsl:for-each select="doc(concat($TEIAlignPath, '/', @alg))//link">
									<xsl:element name="link">
										<xsl:attribute name="id" select="concat($textname, ':', @id)"/>
										<xsl:copy-of select="@certainty" copy-namespaces="no" />
										<xsl:attribute name="xtargets">
											<xsl:value-of select="replace (@xtargets, '(^| |;)([^;])', $repltextname)"/>
										</xsl:attribute>
									</xsl:element>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:result-document>

<!-- sh file..  -->
				<xsl:result-document href="{concat($OUTPUTPATH,'/', $lng1, '-', $lng2, '/',  $lng1, '-', $lng2, '.sh')}">
					<xsl:value-of select="concat('~/uplug/uplug align/word/tagged -in ', $lng1, '-', $lng2, '.xml -out ', $lng1, '-', $lng2 ,'.links')"/>
				</xsl:result-document>





			</xsl:if>
		</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
