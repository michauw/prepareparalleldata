# dieses Modul h�lt zentral alle XML-Strings f�r die RPC-Skripte bereit. �nderungen nur hier.
$TGbegSStart="<s id=\""; 
$TGbegSEnd="\">"; 
$TGendS="</s>";
$TGLemmaStart="<lemma>";
$TGLemmaEnd="</lemma>";

$TGbegDiv="<div>";
$TGendDiv="</div>";

#kept for compatibility
$TGbegAnyTokStart="<tok type=";
$TGbegWordToken="$TGbegAnyTokStart"."\"word\">";
$TGbegOtherToken="$TGbegAnyTokStart"."\"other\">";
$TGendToken="</tok>";

#now
$TGTokenStartRump="<tok";
$TGTokenStart="<tok>";
$TGTokenEnd="</tok>";

$TGbegBody="<body>";
$TGendBody="</body>";
$TGbegTEIheader="<TEI2><teiHeader>";
$TGendTEIheader="</teiHeader><text>\n$TGbegBody";
$TGTEIfooter="$TGendDiv\n$TGendBody\</text></TEI2>";

#builds the header, takes a hash with the following entries:
#  @author
#  @origauthor
#  @translator
#  @title
#  @origtitle
#  @titlecomment
# @year, @origyear, @original
#  @source
#  @publication
# @languagecomment
# @origlanguage
# @original (no argument, used to mark original version)
#@comment
#@writingsystem
sub TGbuildTEIHeader{
  my $header;
  my(%infos) = %{$_[0]};
  
   if (%infos eq "") {print "can't build header!\n"; die;}
  
  #now walk through the hash and escape all what needs to be
  for (keys %infos){
  	#print $infos{$_};
  	$infos{$_}= TGescapeXML ($infos{$_});
  }
  
  
  #TEI header  consists of mandatory filedesc.
  #start building the header
    $header.="$TGbegTEIheader\n<fileDesc>\n<titleStmt>\n";
  #title statement; mandatory presuming something's always there 
  if ($infos{'@title'} ne ""){
  	$header.="<title>$infos{'@title'}<\/title>\n";
  }
  #it's given as a second title statement, no qualifications
  if (($infos{'@origtitle'} ne "") or ($infos{'@originaltitle'} ne "")){
  	$header.="<title>$infos{'@originaltitle'}</title>\n";
  }
  
  if ($infos{'@author'} ne ""){
  	$header.="<author>$infos{'@author'}</author>\n";
  }
  
  #as with title
  if (($infos{'@originalauthor'} ne "") or ($infos{'@origauthor'} ne "")){
  	$header.="<author>$infos{'@originalauthor'}</author>\n";
  }
  
  if ($infos{'@translator'} ne ""){
  	$header.="<respStmt><resp>translation by</resp><name>$infos{'@translator'} $infos{'@translationyear'}<\/name></respStmt>\n";
  }
  
    #end of the title statement 
  $header.="$infos{'@titlecomment'}</titleStmt>\n";
  
  #this part is mandatory: publication statement
  $header .= "<publicationStmt>RPC $infos{'@publication'}</publicationStmt>\n";
  

  #this part is mandatory: source statement
  $header.="<sourceDesc>$infos{'@source'}\n";
  if ($infos{'@year'} ne ""){
	$header.="year=$infos{'@year'}\n";	
  }
  if ($infos{'@origauthor'} ne ""){
	$header.="Original version:\n origauthor=$infos{'@origauthor'}\n";	
  }
  if ($infos{'@origtitle'} ne ""){
	$header.="origtitle=$infos{'@origtitle'}\n";	
  }
  if ($infos{'@origyear'} ne ""){
	$header.="origyear=$infos{'@origyear'}\n";	
  }
  if ($infos{'@origlanguage'} ne ""){
	$header.="origlanguage=$infos{'@origlanguage'}\n";	
  }
  
if ( defined ($infos{'@original'})){
		$header.="Original version.\n"
}


  
  
  $header.="</sourceDesc>\n";
  
  #end of the file description
  $header.="</fileDesc>\n";
  
  
  #profile description, should at least include language
  if ($infos{'@language'} ne ""){
  	$header.="<profileDesc><langUsage><language>$infos{'@language'}";
  		if ($infos{'@writingsystem'} ne ""){
  			$header.="wsd=\"$infos{'@writingsystem'}\"";
  		}
  	$header.="$infos{'@languagecomment'}</language>\n";
	if ($infos{'@origlanguage'} ne ""){
	  	$header.="<language>$infos{'@origlanguage'} (Original version)</language>";
	}
	$header.="</langUsage>\n";
	if ( defined ($infos{'@original'})){
		$header.="Original version.\n"
	}
	if ( defined ($infos{'@comment'})){
		$header.="$infos{'@comment'}\n"
	}

	$header.="</profileDesc>\n";
  }
  
  
  #end of the header
  $header.=$TGendTEIheader;
  
  return $header;
}

#builds the header, takes a hash with the following entries:
#@short the text's invariant nomination (e.g., BoellClown)
#@lng    the text's language (2-letter code)
#@origlng the text's original language (2-letter code)    
#@author author as in this edition     
#@origauthor  author in the original 
#@title     title as in this edition  
#@origtitle title as in the original  
#@year      year of this edition  
#@origyear   year of first publication of the original
#@translator  translator(s)
#@publisher  publishing house 
#@place	place
#@isoriginal  include this if this is the original version
#@endheader obligatory last line of this header
sub TGbuildTable{
  my $header;
  my(%infos) = %{$_[0]};
  my $entry="";
  
  if (%infos eq "") {print "can't build header!\n"; die;}
  #now walk through the hash and escape all what needs to be
  for (keys %infos){
  	#print $infos{$_};
  }



if (defined $infos{'@isoriginal'}){
	$isoriginal=1;
}
else {
	$isoriginal=0;
}


my $short	= $infos{'@short'};
my $lng	= $infos{'@lng'};
my $origlng    = $infos{'@origlng'};
my $author     = $infos{'@author'};
my $origauthor = $infos{'@origauthor'};
my $title      = $infos{'@title'};
my $origtitle  = $infos{'@origtitle'};
my $year       = $infos{'@year'};
my $origyear   = $infos{'@origyear'};
my $translator = $infos{'@translator'};
my $publisher  = $infos{'@publisher'};
my $place      = $infos{'@place'};
my $comment    = $infos{'@comment'};


if (defined $infos{'@translationyear'}){
$year=$infos{'@translationyear'};
}
if (defined $infos{'@source'}){
$publisher=$infos{'@source'};
}

if (defined $infos{'@language'}){
	$lng=&TGgetLngShort($infos{'@language'});
}
if (defined $infos{'@origlanguage'}){
	$origlng=&TGgetLngShort($infos{'@origlanguage'});
}

if (defined $infos{'@originallanguage'}){
	$origlng=&TGgetLngShort($infos{'@originallanguage'});
}



#fill in blanks for originals
if (defined $infos{'@isoriginal'}){
	if ($origtitle eq "") {$origtitle=$title;}
	if ($origauthor eq "") {$origauthor=$author;};
	if ($origyear eq "") {$origyear=$year;};
	if ($origlng eq "") {$origlng=$lng;};

	if ($title eq "") {$title=$origtitle;};
	if ($author eq "") {$author=$origauthor;};
	if ($year eq "") {$year=$origyear;};
	if ($lng eq "") {$lng=$origlng;};
}



$entry="$short\t$lng\t$origlng\t$isoriginal\t$author\t$origauthor\t$title\t$origtitle\t$year\t$origyear\t$translator\t$publisher\t$place\t\n";
return $entry;
    
}





sub TGgetToken{
	#print "othertoken! $_[0]\n";
	return "$TGTokenStart$_[0]$TGTokenEnd"; 
}



$TGblockTag="<BLOCK>";

#Tags
$TGTag1Start="<tag1>";
$TGTag1End="<\/tag1>";
$TGTag2Start="<tag2>";
$TGTag2End="<\/tag2>";
$TGTag3Start="<tag3>";
$TGTag3End="<\/tag3>";
$TGTag4Start="<tag4>";
$TGTag4End="<\/tag4>";
$TGTag5Start="<tag5>";
$TGTag5End="<\/tag5>";


#getroutines
 sub TGgetSTag{
 	return "$TGbegSStart$_[0]$TGbegSEnd";
 }


#f�r alignierungsdatei: alt
$TGbeadTypeStart="<type=";
$TGbeadTypeEnd="\>";
$TGAligStart="<Alig Ln1=";
$TGAligEnd="\>";
$TGAligLng2=" Ln2=";
$TGAligID=" id=";

#This are the new versions...
#syntax: start x, starty, endx, endy, score/type
sub TGgetAlignmentBead{
	return "<alig Ln1Strt=\"$_[0]\" Ln2Strt=\"$_[1]\" Ln1End=\"$_[2]\" Ln2End=\"$_[3]\" type=\"$_[4]\"/>\n";
}

#This are the new versions...
sub TGdecodeAlignmentBead{
	
	if ($_[0] =~ /<alig Ln1Strt=\"(\d+)\" Ln2Strt=\"(\d+)\" Ln1End=\"(\d+)\" Ln2End=\"(\d+)\" type=\"(.*?)\"\/>/){

		return ($1, $2, $3, $4, $5);
	} 
	else	
	{
		print "ERROR decoding alignment:";
		for $arg (@_){
			print $arg." ";
		}
		print "\n";
		return "";
	}	
}



$TGbeadTypeStartNN="<type=";
$TGbeadTypeEndNN="\>";
$TGAligStartNN="<Alig Ln1=";
$TGAligEndNN="\>";
$TGAligLng2NN=" Ln2=";
$TGAligIDNN=" id=";





#8-bitCodierung f�r Paraconc etc
sub TGgetWinEncoding{
	my $language=$_[0];
		return ":encoding(".&TGgetBareWinEncoding($language).")";
}

#just return the string
sub TGgetBareWinEncoding{
	my $enc ="";
	if ($_[0] =~ /BY|RU|BG|UK|SC|SB/){
		$enc= "cp1251";
	}
	if ($_[0] =~ /PL|CZ|SK|HR|SL|SX|YU|BX1|BX|BX2/){
		$enc= "cp1250";
	}
	if ($_[0] =~ /FR|IT|EN|DE/){
		$enc= "cp1252";
	}
	#default
	if (($enc eq "")  or (not defined $enc)){
	$enc= "utf8";
	}
	return $enc;
}


sub TGgetLngFullName{
$full="Bulgarian" if ($_[0] =~ /BG/);
$full="Belarusian" if ($_[0] =~ /BY/);
$full="Czech" if ($_[0] =~ /CZ/);
$full="German" if ($_[0] =~ /DE/);
$full="Greek" if ($_[0] =~ /EL/);
$full="English" if ($_[0] =~ /EN/);
$full="Esperanto" if ($_[0] =~ /EO/);
$full="Spanish" if ($_[0] =~ /ES/);
$full="French" if ($_[0] =~ /FR/);
$full="Croatian" if ($_[0] =~ /HR/);
$full="Hungarian" if ($_[0] =~ /HU/);
$full="Italian" if ($_[0] =~ /IT/);
$full="Lithuanian" if ($_[0] =~ /LT/);
$full="Latvian" if ($_[0] =~ /LV/);
$full="Macedonian" if ($_[0] =~ /MK/);
$full="Dutch" if ($_[0] =~ /NL/);
$full="Polish" if ($_[0] =~ /PL/);
$full="Portuguese" if ($_[0] =~ /PT/);
$full="Romanian" if ($_[0] =~ /RO/);                               
$full="Russian" if ($_[0] =~ /RU/);
$full="Upper Sorbian" if ($_[0] =~ /US/);
$full="Slovak" if ($_[0] =~ /SK/);
$full="Slovene" if ($_[0] =~ /SL/);
$full="Serbian" if ($_[0] =~ /SR/);
$full="Ukrainian" if ($_[0] =~ /UK/);
$full="Danish" if ($_[0] =~ /DA/);
$full="Finnish" if ($_[0] =~ /FI/);
$full="Swedish" if ($_[0] =~ /SV/);
$full="Armenian" if ($_[0] =~ /HY/);
$full="Estonian" if ($_[0] =~ /EE/);
$full="Norwegian" if ($_[0] =~ /NO/);
	#alternatives
	if ($_[0] =~ /(..)([a-z])/){
		$full .=" - $2";
	}
	return $full;
}




sub TGgetLngShort{
	my $short;
	$short ="EN" if ($_[0] =~ /English/);
	$short ="SK" if ($_[0] =~ /Slovak/);
	$short ="CZ" if ($_[0] =~ /Czech/);
	$short ="DE" if ($_[0] =~ /German/);
	$short ="RU" if ($_[0] =~ /Russian/);
	$short ="UK" if ($_[0] =~ /Ukrainian/);
	$short ="BY" if ($_[0] =~ /Belorussian/);
	$short ="BG" if ($_[0] =~ /Bulgarian/);
	$short ="FR" if ($_[0] =~ /French/);
	$short ="PL" if ($_[0] =~ /Polish/);
	$short ="BX1" if ($_[0] =~ /Serbian/);
	$short ="BX2" if ($_[0] =~ /Croatian/);
	$short ="BX" if ($_[0] =~ /BosnianCroatianSerbian/);#maybe later use this...
	$short ="YU" if ($_[0] =~ /BosnianCroatianSerbian/);#maybe later use this...
	$short ="HR" if ($_[0] =~ /Croatian/);
	return $short;
}

#escape and unescape routines for XML!
#takes a string, escapes any XML-characters and returns the result
sub TGescapeXML{
 	$word=$_[0];
 	#print "converting: $word to ";
	$word =~s/\\/\\\\/g;		#slash (\) 	\\
	$word =~s/\&/\&amp\;/g; #ampersand (\&) 	\&amp;
	$word =~s/\"/&quot;/g;			#	quote (") 	\"
	#$word =~s/\"/\\\"/g;			#	quote (") 	\"
	$word =~s/\'/\&apos\;/g;   #apostrophe (') 	\&apos;
	$word =~s/\</\&lt\;/g;#less than (<) 	\&lt;
	$word =~s/\>/\&gt\;/g;#greater than (>) 	\&gt;
	#print "$word!\n";
	return $word;
}
#takes a string, unescapes any XML-characters and returns the result
sub TGunescapeXML{
 	$word=$_[0];
	$word =~s/\\\\/\\/g;		#slash (\) 	\\
	$word =~s/\&amp\;/\&/g; #ampersand (\&) 	\&amp;
	$word =~s/\\\"/\"/g;			#	quote (") 	\"
	$word =~s/&quot;/\"/g;			#	quote (") 	\"
	$word =~s/\&apos\;/\'/g;   #apostrophe (') 	\&apos;
	$word =~s/\&lt\;/\</g;#less than (<) 	\&lt;
	$word =~s/\&gt\;/\>/g;#greater than (>) 	\&gt;
	return $word;
}

sub TGgetLngTagSetURL{
$full='http://search.dcl.bas.bg/' if ($_[0] =~ /BG/);
$full='' if ($_[0] =~ /BY/);
$full='http://ufal.mff.cuni.cz/pdt/Morphology_and_Tagging/Doc/hmptagqr.html' if ($_[0] =~ /CZ/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /DE/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /EL/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /EN/);
$full='' if ($_[0] =~ /EO/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /ES/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /FR/);
$full='' if ($_[0] =~ /HR/);
$full='' if ($_[0] =~ /HU/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /IT/);
$full='' if ($_[0] =~ /LT/);
$full='' if ($_[0] =~ /LV/);
$full='' if ($_[0] =~ /MK/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /NL/);
$full='http://korpus.pl/en/cheatsheet/index.html' if ($_[0] =~ /PL/);
$full='http://www.ims.uni-stuttgart.de/projekte/corplex/TreeTagger/' if ($_[0] =~ /PT/);
$full='' if ($_[0] =~ /RO/);                               
$full='http://corpus.leeds.ac.uk/mocky/msd-ru.html' if ($_[0] =~ /RU/);
$full='http://korpus.sk/morpho.html/' if ($_[0] =~ /SK/);
$full='' if ($_[0] =~ /SL/);
$full='' if ($_[0] =~ /SR/);
$full='' if ($_[0] =~ /UK/);
$full='' if ($_[0] =~ /DA/);
$full='' if ($_[0] =~ /FI/);
$full='' if ($_[0] =~ /SV/);
$full='' if ($_[0] =~ /HY/);
$full='' if ($_[0] =~ /EE/);
$full='' if ($_[0] =~ /NO/);
	#alternatives
	if ($_[0] =~ /(..)([a-z])/){
		$full .=" - $2";
	}
	return $full;
}

