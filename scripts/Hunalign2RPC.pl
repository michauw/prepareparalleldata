use RPC_XML_Tags;
#binmode(STDOUT, ":cp1252");

use File::Spec::Functions;
use Cwd;
use open ':utf8';

#zunächst Argument parsen
#alle Argumente in einen Hash, dann sind sie leicht abzufragen
for $par (@ARGV){
	#parameterübergabe?
	if ($par  =~ /^(.+)=(.+)$/){
		$args{$1}=$2;
	}
	else {#Schalter bzw. Dateiname
		$args{$par}=1;
	}
}


if (defined $args{'-idir'}){
	$idir=$args{'-idir'};
} 
else {
	$idir=".";
}

if (defined $args{'-odir'}){
	$odir=$args{'-odir'};
} 
else {
	$odir="ARBEIT";
}

if (defined $args{'-rpcDir'}){
	$rpcdir=$args{'-rpcDir'};
} 
else {
	$rpcdir="Texte-rpc";
}


if ($args{-silent}==0){
  print "This script converts the numerical alignment output of hunalign and converts it to the RPC alignment format.\n";
  print "Files should be named RUMP_LNG-LNG.HUN. They will be converted to RUMP_LNG-LNG.ALG\n"; 
  print "The scripts determines the set of all files according to this convention in the input directory and converts  \nthem using the rpc files in the specified rpc directory.\n";
  print "If -multidir is specified, it will write the alignment files to subdirecories of the outputdirectory\n";
  print "Arguments: -idir=InputDirectory -odir=OutputDirectory -rpcDir=RpcDirectory (XML-files) [-silent]\n\n";
}

$Hext='hun';

#Alle Dateien im Verzeichnis
(opendir DIR, $idir) || die("Could not open directory $idir\n"); 
		@all_hun_files = grep /\.$Hext$/, readdir DIR;
closedir(DIR);

print "found ".(@all_hun_files+0)." hun files in $idir!!\n";

#create outputdir
print "Hooo hoooo!!!\n$odir";
opendir (DIR, $odir) or mkdir ($odir);

foreach $file_name (@all_hun_files) {
	#clear all arrays
	@offsets=();
	@Xrung  =();
	@Yrung  =();
	@score  =();
	
	
	$hunalgfile=catfile ($idir, $file_name);

	$file_name =~ /(.*)_(.+?)-(.+?)\.$Hext$/;
	$rpcfile = "$1_$2.rpc";

	$rump=$1;

	$ln[0]=$2;
	$ln[1]=$3;
	@ln=(sort @ln);

	$rpcfile =catfile ($rpcdir, $rpcfile);
	

	#two ways: simple or multidir
	unless (defined $args{'-multidir'}){
		$outalgfile="$rump\_$ln[0]-$ln[1].alg";
		$outalgfile= catfile ($odir, $outalgfile);
	}
	else{
		

		#get the short version of the language tags
		$lnshort[0]=$ln[0]; $lnshort[0] =~ s/^(..).?$/$1/;
		$lnshort[1]=$ln[1]; $lnshort[1] =~ s/^(..).?$/$1/;

		#now distribute to directories; if these are different languages, the dir is clear:
		if ($lnshort[0] ne $lnshort[1]){
			$subdir="$lnshort[0]-$lnshort[1]-ALG";
			#in this case, check for variants uzsed
			#if so, change the name
			if (($ln[0] ne $lnshort[0]) or ($ln[1] ne $lnshort[1])){
				 $rump="$rump=$ln[0]=$ln[1]=";
			}
      		#the name is expanded with short versions in any case
      		$combo="$lnshort[0]-$lnshort[1]";
		}else{
			#these are variants of the same language; move them to the full-name directory
			$subdir="$ln[0]-$ln[1]-ALG";
			#the name is kept
      		$combo="$ln[0]-$ln[1]";
      	}

		
		$subdir= catdir ($odir, $subdir);
		#now check for dirs, if necessary, create them
		opendir (DIR, $subdir) or mkdir ($subdir);
		     
		#now create the file name     
		$outalgfile=catfile($subdir, "$rump\_$combo.alg");
	}


	#go through the rpc file and get the div positions
	#each of them will increase the Hunalignoutput sentence count by one
	open (RPC, "<$rpcfile") or die ("no luck opening $rpcfile!"); 
	
	#first move up to the first sentence...
	$sent=-1;
	for (<RPC>){
		if (($_=~ /$TGendDiv/)&($sent>-1)){
			#check whether maybe there was a sentence marker before the div
			#might miss a div that's immediately after line 1 and in the same line
			#shouldn't happen if </div> is always followed by line break
			if ($_=~ /$TGbegSStart(\d+)$TGbegSEnd.*?$TGendDiv/){
				$sent=$1;
			}
			push (@offsets, $sent);
			print "div after $sent found!\n"; 
		}
		if ($_=~ /$TGbegSStart(\d+)$TGbegSEnd/){
			$sent=$1;
		}
		$last=$_;
	}
	
	#push the last, too...
	push (@offsets, $sent+1);
	
	#testing:
	# for (@offsets){ print "$_\t";} print "\n";
	
	#now delete the last one (leftover ?);
	#pop (@offsets);
	
	
	open (HUN, "<$hunalgfile") or die("opening $hunalgfile");  
	open (OUT, ">$outalgfile") or die ("opening $outalgfile"); 
	
	
	#OK, walk through the alignment file, adjusting the numbers according to these offsets
	$offsetcounter=0;
#	#skip the first line, always 0 0
#	$dum=<HUN>;
	$Xrung=0;
	$Yrung=0;
	$LastXrung=$Xrung;
	$LastYrung=$Yrung;
	
	#read alignment in arrays
	for $alg (<HUN>){
		if ($alg=~ /^(\d+)\t(\d+)\t([-.\de]+)$/){
		
			$Xrung=$1-$offsetcounter;
			$Yrung=$2-$offsetcounter;
			$score=$3;
			
			#OK, check for offsets.
			#We can simply take them out
			if (($Xrung<=$offsets[$offsetcounter])or ($offsetcounter==$#offsets)){
				push (@Xrung, $Xrung);
				push (@Yrung, $Yrung);
				push (@score, $score);
			}else{
				$offsetcounter++;
			}	
		}
	}
			
		
	
	for $rt(0..($#Xrung-1)){
			
		$Xrung=$Xrung[$rt];
		$NextXrung=$Xrung[$rt+1];
		
		$Yrung=$Yrung[$rt];
		$NextYrung=$Yrung[$rt+1];
		
		$score=$score[$rt];
		
		$newalignment=&TGgetAlignmentBead($Xrung,$Yrung, $NextXrung, $NextYrung, $score);
		print OUT $newalignment;
		#checking whether we have moved BEYOND the last line before DIV
	}
			
}

exit;



