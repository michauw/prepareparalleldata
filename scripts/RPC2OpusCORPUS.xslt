<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
  <!-- Filter version 2.2.0_qdxml - 2010-12-19 - vjl -->
	<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>
	<!-- <xsl:strip-space elements="*"/>-->  
	<xsl:preserve-space elements="*"/>

	<xsl:param name="short" select="'no'"/>
	<xsl:param name="transformPL" select="'no'"/>
	<xsl:param name="lemma" select="'no'"/>
	<xsl:param name="deleteNsp" select="'yes'"/>
	
	<!--
	<xsl:variable name="short">
	 <no></no>
	<yes></yes>
	</xsl:variable>

	<xsl:variable name="lemma">
	<no></no>
	  <yes></yes>
	  <no></no>
	</xsl:variable>
	-->

	
<xsl:template match="/">
<xsl:message>processing file...</xsl:message>
<xsl:element name="text">
<xsl:apply-templates select="./TEI2/text/body"/>
</xsl:element>
</xsl:template>

<xsl:template match="div">
<xsl:element name="p">
<xsl:attribute name="id">
<xsl:number/>
</xsl:attribute>
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="s">
<xsl:element name="s">
<xsl:attribute name="id" select="@id"/>
<xsl:if test="not ($deleteNsp eq 'yes')">
<xsl:apply-templates/>
</xsl:if>
<xsl:if test="($deleteNsp eq 'yes')">
<xsl:apply-templates select="tok[not ((./lemma eq 'nsp') and (./tag1 eq 'nsp') and (text() eq '|'))]"/>
</xsl:if>
</xsl:element>
</xsl:template>

<xsl:template match="tok">
<!-- ignore nsp tokens if so specified -->
<xsl:element name="w">
<xsl:attribute name="id" select="concat(string(../@id), '.', position())"/>
<xsl:attribute name="lem" select="./lemma"/>
<xsl:attribute name="pos">
<xsl:if test="$short eq 'yes'">
<!-- we shorten and for Polish, we transform
<xsl:value-of select="replace(./tag1, '^(.).*', '$1')"/>
</xsl:if>
<xsl:if test="$short eq 'PL'">-->
<xsl:value-of select="
if (matches (./tag1, '^(subst|depr|ger|xxs)')) then ('N') else (
if (matches (./tag1, '^(fin|praet|inf|imps|impt|pact|ppas|pcon|pant|ger)')) then ('V') else (
if (matches (./tag1, '^(ppron12|ppron3|siebie)')) then ('P') else (
if (matches (./tag1, '^(aglt|bedzie)')) then ('Q') else (

if (matches (./tag1, '^(bdvr)')) then ('A') else (
if (matches (./tag1, '^(idJngt|jngt)')) then ('C') else (
if (matches (./tag1, '^(punct)')) then ('I') else (
if (matches (./tag1, '^(prvks)')) then ('J') else (
if (matches (./tag1, '^(įvrd)')) then ('M') else (
if (matches (./tag1, '^(dktv|tikr)')) then ('N') else (
if (matches (./tag1, '^(idPrln|prln)')) then ('P') else (
if (matches (./tag1, '^(akronim|sntrmp)')) then ('Q') else (
if (matches (./tag1, '^(bndr|būdn|dlv|padlv|psdlv|vksm)')) then ('V') else (
if (matches (./tag1, '^(dll|ištk)')) then ('X') else (
if (matches (./tag1, '^(num|rom|sktv)')) then ('Z') else (


replace(./tag1, '^(.).*', '$1'))
)
)
)
)))))))))))
"/>
</xsl:if>
<xsl:if test="$short eq 'no'">
<xsl:value-of select="./tag1"/>
</xsl:if>
</xsl:attribute>
<xsl:if test="$lemma eq 'yes'">
<xsl:value-of  select="./lemma"/>
</xsl:if>
<xsl:if test="$lemma eq 'no'">
<xsl:value-of  select="text()"/>
</xsl:if>
</xsl:element>
</xsl:template>



</xsl:stylesheet>
