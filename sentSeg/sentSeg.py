import sys, os
from nltk.tokenize import sent_tokenize as st

if __name__ == '__main__':
    if len (sys.argv) < 2:
        print 'No input data'
        sys.exit (0)
    PATH = sys.argv[1]
    if not os.path.isfile (PATH):
        print 'Wrong path, sorry!'
        sys.exit (0)
    with open (PATH) as f:
        text = f.read ().decode ('utf8')
    tokenized = st (text)
    if len (sys.argv) > 2:
        output = sys.argv[2]
    else:
        name, ext = os.path.splitext (PATH)
        output = name + '-sentAl' + ext
    with open (output, 'w') as f:
        for sent in tokenized:
            f.write (sent.encode ('utf8') + '\n')
    print 'done.'
